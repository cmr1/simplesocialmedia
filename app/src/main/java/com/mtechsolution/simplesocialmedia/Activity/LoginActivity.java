package com.mtechsolution.simplesocialmedia.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mtechsolution.simplesocialmedia.R;

import java.util.Calendar;

public class LoginActivity extends AppCompatActivity {

    private TextView mTvForgotPassword;
    private Button mBtnRegister,mBtnLogin;
    private EditText mEdtEmail,mEdtPassword;
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private String mCurrentUserId;
    private DatabaseReference mRootRef;
    private ProgressDialog mLoadingBar;
    private SharedPreferences mSharePref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mTvForgotPassword=(TextView) findViewById(R.id.tvforgotpassword);
        mBtnRegister=(Button) findViewById(R.id.btnregister);
        mBtnLogin=(Button) findViewById(R.id.btnlogin);
        mEdtEmail=(EditText) findViewById(R.id.edtemail);
        mEdtPassword=(EditText) findViewById(R.id.edtpassword);


        mLoadingBar=new ProgressDialog(this);
        mSharePref = getSharedPreferences("socialmediaapp", MODE_PRIVATE);
        mAuth=FirebaseAuth.getInstance();
        mRootRef= FirebaseDatabase.getInstance().getReference();

        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email=mEdtEmail.getText().toString();
                String password=mEdtPassword.getText().toString();
                if (email.isEmpty()){
                    Toast.makeText(LoginActivity.this, "por favor ingrese correo electrónico", Toast.LENGTH_SHORT).show();
                } else if (password.isEmpty()){
                    Toast.makeText(LoginActivity.this, "Por favor, ingrese contraseña", Toast.LENGTH_SHORT).show();
                } else {
                    Login(email,password);
                }
            }
        });

        mBtnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });

        mTvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LoginActivity.this,ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

    }

    private void Login(final String email, final String password){
        ShowDialog("Por favor espere a que inicie sesión en progreso...");
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            mCurrentUser = mAuth.getCurrentUser();
                            mCurrentUserId = mCurrentUser.getUid();
                            mRootRef.child("Users").child(mCurrentUserId)
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()){
                                        String getdob=dataSnapshot.child("dob").getValue().toString();
                                        String getgender=dataSnapshot.child("gender").getValue().toString();
                                        String getimage=dataSnapshot.child("image").getValue().toString();
                                        String getname=dataSnapshot.child("name").getValue().toString();

                                        SharedPreferences.Editor editor= mSharePref.edit();
                                        editor.putString("name", getname);
                                        editor.putString("email", email);
                                        editor.putString("dob", getdob);
                                        editor.putString("password", password);
                                        editor.putString("gender",getgender);
                                        editor.putString("image",getimage);
                                        editor.putString("userid",mCurrentUserId);
                                        editor.apply();

                                        Intent it=new Intent(LoginActivity.this,MainActivity.class);
                                        startActivity(it);
                                        finish();
                                        HideDialog();

                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                        } else {
                            Toast.makeText(getApplicationContext(), "¡Error de inicio de sesion! Por favor, inténtelo de nuevo más tarde", Toast.LENGTH_LONG).show();
                            HideDialog();
                        }
                    }
                });
    }
    private void ShowDialog(String message) {
        if (mLoadingBar != null && !mLoadingBar.isShowing()) {
            mLoadingBar.setCancelable(true);
            mLoadingBar.setMessage(message);
            mLoadingBar.show();
        }
    }

    private void HideDialog() {
        if (mLoadingBar != null && mLoadingBar.isShowing()) {
            mLoadingBar.dismiss();
        }
    }
}
package com.mtechsolution.simplesocialmedia.Fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mtechsolution.simplesocialmedia.Activity.MyPostsActivity;
import com.mtechsolution.simplesocialmedia.Activity.ProfileActivity;
import com.mtechsolution.simplesocialmedia.R;
import com.skydoves.powerspinner.PowerSpinnerView;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.volokh.danylo.hashtaghelper.HashTagHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

public class AddPostFragment extends Fragment implements BillingProcessor.IBillingHandler {

    private ImageView mImagePost;
    private Button mBtnPost,mBtnPremium;
    private EditText mEdtTitle,mEdtCompaignURL,mEdtPostDescription,mEdtHashtag;
    private PowerSpinnerView mSpPostCategory;
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private String mCurrentUserId;
    private DatabaseReference mRootRef;
    private StorageReference mStorageRef;
    private static final int PERMISSION_GRAUNTED=555;
    private ProgressDialog mLoadingBar;
    private Uri mResultUri;
    private boolean mPickImage=false;
    private HashTagHelper mTextHashTagHelper;
    private SharedPreferences mSharePref;
    private BillingProcessor bp;
    private FloatingActionButton mFabMyPosts;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_add_post, container, false);

        mFabMyPosts=(FloatingActionButton) root.findViewById(R.id.fabmyposts);
        mImagePost=(ImageView) root.findViewById(R.id.imagepost);
        mBtnPost=(Button) root.findViewById(R.id.btnsubmit);
        mBtnPremium=(Button) root.findViewById(R.id.btnpremium);
        mEdtTitle=(EditText) root.findViewById(R.id.edtpostitle);
        mEdtCompaignURL=(EditText) root.findViewById(R.id.edtpostcompaignurl);
        mEdtPostDescription=(EditText) root.findViewById(R.id.edtpostdescription);
        mEdtHashtag=(EditText) root.findViewById(R.id.edtposthashtag);
        mSpPostCategory=(PowerSpinnerView) root.findViewById(R.id.sppostcategory);

        bp = new BillingProcessor(getContext(), "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtEddbr7wOO8q1w9SDk9RTRzkCR+TET/lruKNys4GHknT3d91OeJxcVaWL/oL/x+phmGl/8qnNd2+Us2Fy331a7U841eH/tyzKfgdpmtrteVVgOar3KcRYzLUHfHShU3LVy5Vc47jrk7arpiyrgDwvHUcS10hPbmY1A7oYKpkXeTLMdHnpTPOIu5Hwy9bibckRc+LJpSG4yhD9+NF9ZstoE8eh/03Q+6kqPdc6oplyRtekkbidaLA006jSvo2z3djb1nMsYlLWGrSvMkiS2wzJDtUlGS2exo827KLnEH7p1oXO3JVj5Fo8UH8LtkO0Tr81R0KosKG1XaWvMHhylvQIQIDAQAB", this);
        bp.initialize();
        mLoadingBar=new ProgressDialog(getContext());
        mAuth=FirebaseAuth.getInstance();
        mCurrentUser=mAuth.getCurrentUser();
        mCurrentUserId=mCurrentUser.getUid();
        mRootRef= FirebaseDatabase.getInstance().getReference();
        mStorageRef = FirebaseStorage.getInstance().getReference().child("Publicar imágenes");
        mSharePref = getContext().getSharedPreferences("socialmediaapp", MODE_PRIVATE);

        mTextHashTagHelper = HashTagHelper.Creator.create(getResources().getColor(R.color.colorPrimary), new HashTagHelper.OnHashTagClickListener() {
            @Override
            public void onHashTagClicked(String hashTag) {

            }
        });
        mTextHashTagHelper.handle(mEdtHashtag);

        mImagePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckAppPermissions();
            }
        });

        mBtnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String getTitle=mEdtTitle.getText().toString();
                String getDescription=mEdtPostDescription.getText().toString();
                String getCompaingnUrl=mEdtCompaignURL.getText().toString();
                String getHashtag=mEdtHashtag.getText().toString();
                String getCategory=mSpPostCategory.getText().toString();

                if (getTitle.isEmpty()) {
                    Toast.makeText(getContext(), "Por favor ingrese el título", Toast.LENGTH_SHORT).show();
                } else if (getDescription.isEmpty()) {
                    Toast.makeText(getContext(), "Por favor ingrese descripción", Toast.LENGTH_SHORT).show();
                } else if (getHashtag.isEmpty()) {
                    Toast.makeText(getContext(), "Por favor ingrese el hashtag", Toast.LENGTH_SHORT).show();
                } else if (getCategory.isEmpty()) {
                    Toast.makeText(getContext(), "Por favor ingrese categoría", Toast.LENGTH_SHORT).show();
                } else if (!mPickImage) {
                    Toast.makeText(getContext(), "Por favor elija la imagen", Toast.LENGTH_SHORT).show();
                } else {
                    if (getCompaingnUrl.isEmpty()){
                        getCompaingnUrl="Sin sitio web";
                    }
                    AddPost(getTitle,getDescription,getCompaingnUrl,getHashtag,getCategory,mResultUri,"Simple");
                }
            }
        });

        mFabMyPosts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(), MyPostsActivity.class);
                startActivity(intent);
            }
        });
        mBtnPremium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String getTitle=mEdtTitle.getText().toString();
                String getDescription=mEdtPostDescription.getText().toString();
                String getCompaingnUrl=mEdtCompaignURL.getText().toString();
                String getHashtag=mEdtHashtag.getText().toString();
                String getCategory=mSpPostCategory.getText().toString();

              if (getTitle.isEmpty()) {
                    Toast.makeText(getContext(), "Por favor ingrese el título", Toast.LENGTH_SHORT).show();
                } else if (getDescription.isEmpty()) {
                    Toast.makeText(getContext(), "Por favor ingrese descripción", Toast.LENGTH_SHORT).show();
                } else if (getHashtag.isEmpty()) {
                    Toast.makeText(getContext(), "Por favor ingrese el hashtag", Toast.LENGTH_SHORT).show();
                } else if (getCategory.isEmpty()) {
                    Toast.makeText(getContext(), "Por favor ingrese categoría", Toast.LENGTH_SHORT).show();
                } else if (!mPickImage) {
                    Toast.makeText(getContext(), "Por favor elija la imagen", Toast.LENGTH_SHORT).show();
              } else {
                  if (getCompaingnUrl.isEmpty()){
                      getCompaingnUrl="Sin sitio web";
                  }
                bp.purchase(getActivity(), "premiumpost");
            }
            }
        });

        return root;
    }

    private void AddPost(final String getTitle, final String getDescription, final String getCompaignUrl,
                         final String getHashtag, final String getCategory, final Uri mResultUri,final String getPostType){
        ShowDialog("Por favor, espera");
        StorageReference filePath = mStorageRef.child(mCurrentUserId + ".jpg");
        filePath.putFile(mResultUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                if (taskSnapshot.getMetadata() != null) {
                    if (taskSnapshot.getMetadata().getReference() != null) {
                        final Task<Uri> result = taskSnapshot.getStorage().getDownloadUrl();
                        result.addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                final String imageUrl = uri.toString();

                                //Get Post Data
                                Calendar calendar = Calendar.getInstance();
                                String myFormat = "MM/dd/yy"; //In which you need put here
                                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                                HashMap<String,String> hashMap=new HashMap<>();
                                hashMap.put("username",mSharePref.getString("name","null"));
                                hashMap.put("userimage",mSharePref.getString("image","null"));
                                hashMap.put("title",getTitle);
                                hashMap.put("compaignurl",getCompaignUrl);
                                hashMap.put("hashtag",getHashtag);
                                hashMap.put("category",getCategory);
                                hashMap.put("description",getDescription);
                                hashMap.put("postimage",imageUrl);
                                hashMap.put("userid",mCurrentUserId);
                                hashMap.put("date",sdf.format(calendar.getTime()));

                                String pushId = mRootRef.child("Posts").child(getPostType).push().getKey();

                                mRootRef.child("Posts").child(getPostType).child(pushId).setValue(hashMap)
                                        .addOnCompleteListener(new OnCompleteListener() {
                                            @Override
                                            public void onComplete(@NonNull Task task) {
                                                if (task.isSuccessful()){
                                                    HideDialog();
                                                    mEdtTitle.setText("");
                                                    mEdtPostDescription.setText("");
                                                    mEdtHashtag.setText("");
                                                    mEdtCompaignURL.setText("");
                                                    mPickImage=false;
                                                    Picasso.with(getContext()).load(R.drawable.ic_image).into(mImagePost);
                                                    Toast.makeText(getContext(), "Publicación añadida correctamente", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    HideDialog();
                                                    Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                            }
                        });
                    }
                }
            }
        });

    }
    //create method in main
    private void CheckAppPermissions()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try
            {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA}, PERMISSION_GRAUNTED);
            }
            catch (Exception ex)
            {

            }
        }
        else
        {
            PickImageFromGallery();
        }
    }
    private void PickImageFromGallery()
    {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(getContext(),this);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
               mPickImage=true;
               mResultUri = result.getUri();
                mImagePost.setImageURI(mResultUri);

            }

        }
        if (!bp.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSION_GRAUNTED && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            PickImageFromGallery();
        }
        else
        {
            CheckAppPermissions();
        }
    }

    @Override
    public void onDestroy() {
        if (bp != null) {
            bp.release();
        }
        super.onDestroy();
    }
    private void ShowDialog(String message) {
        if (mLoadingBar != null && !mLoadingBar.isShowing()) {
            mLoadingBar.setCancelable(true);
            mLoadingBar.setMessage(message);
            mLoadingBar.show();
        }
    }

    private void HideDialog() {
        if (mLoadingBar != null && mLoadingBar.isShowing()) {
            mLoadingBar.dismiss();
        }
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {
            bp.consumePurchase("premiumpost");
        String getTitle=mEdtTitle.getText().toString();
        String getDescription=mEdtPostDescription.getText().toString();
        String getCompaingnUrl=mEdtCompaignURL.getText().toString();
        String getHashtag=mEdtHashtag.getText().toString();
        String getCategory=mSpPostCategory.getText().toString();

        if (getTitle.isEmpty()) {
            Toast.makeText(getContext(), "Por favor ingrese el título", Toast.LENGTH_SHORT).show();
        } else if (getDescription.isEmpty()) {
            Toast.makeText(getContext(), "Por favor ingrese descripción", Toast.LENGTH_SHORT).show();
        } else if (getHashtag.isEmpty()) {
            Toast.makeText(getContext(), "Por favor ingrese el hashtag", Toast.LENGTH_SHORT).show();
        } else if (getCategory.isEmpty()) {
            Toast.makeText(getContext(), "Por favor ingrese categoría", Toast.LENGTH_SHORT).show();
        } else if (!mPickImage) {
            Toast.makeText(getContext(), "Por favor elija la imagen", Toast.LENGTH_SHORT).show();
        } else {
            if (getCompaingnUrl.isEmpty()){
                getCompaingnUrl="Sin sitio web";
            }
            AddPost(getTitle,getDescription,getCompaingnUrl,getHashtag,getCategory,mResultUri,"Premium");
        }
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {

    }

    @Override
    public void onBillingInitialized() {

    }
}
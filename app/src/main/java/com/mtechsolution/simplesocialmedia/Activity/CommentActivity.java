package com.mtechsolution.simplesocialmedia.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mtechsolution.simplesocialmedia.Fragment.NotificationsFragment;
import com.mtechsolution.simplesocialmedia.Fragment.SearchFragment;
import com.mtechsolution.simplesocialmedia.Model.CommentModel;
import com.mtechsolution.simplesocialmedia.Model.NotificationModel;
import com.mtechsolution.simplesocialmedia.Model.PostModel;
import com.mtechsolution.simplesocialmedia.R;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommentActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private String getPostTitle,getPostId;
    private EditText mEdtComment;
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private String mCurrentUserId;
    private RecyclerView mRecyclerView;
    private DatabaseReference mRootRef;
    private FirebaseRecyclerAdapter<CommentModel, ViewHolder> mAdapter;
    private ProgressDialog mLoadingBar;
    private FloatingActionButton mFabSend;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        getPostTitle=getIntent().getExtras().getString("posttitle");
        getPostId=getIntent().getExtras().getString("postid");

        mToolbar=(Toolbar) findViewById(R.id.toolbar);
        mEdtComment=(EditText) findViewById(R.id.edtcomment);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerviewcomment);
        mFabSend=(FloatingActionButton) findViewById(R.id.fabsendcomment);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getPostTitle);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mAuth= FirebaseAuth.getInstance();
        mCurrentUser=mAuth.getCurrentUser();
        mCurrentUserId = mCurrentUser.getUid();
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mLoadingBar = new ProgressDialog(this);

        mRecyclerView.setHasFixedSize(true);
        //set layout manager to recycler view
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        FirebaseRecyclerOptions<CommentModel> options =
                new FirebaseRecyclerOptions.Builder<CommentModel>()
                        .setQuery(mRootRef.child("Comments").child(getPostId), CommentModel.class)
                        .build();

        mAdapter =
                new FirebaseRecyclerAdapter<CommentModel, ViewHolder>(options) {
                    @Override
                    protected void onBindViewHolder(@NonNull final ViewHolder holder, final int position, @NonNull final CommentModel model) {
                        holder.comment.setText(model.getUsercomment());
                        mRootRef.child("Users").child(model.getUserid())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            String userName=dataSnapshot.child("name").getValue().toString();
                                            String userImage=dataSnapshot.child("image").getValue().toString();
                                            holder.name.setText(userName);
                                            Picasso.with(CommentActivity.this).load(userImage)
                                                    .placeholder(R.drawable.ic_user).into(holder.image);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                    }

                    @NonNull
                    @Override
                    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                        View view = LayoutInflater.from(viewGroup.getContext())
                                .inflate(R.layout.layoutcomment, viewGroup, false);
                        ViewHolder viewHolder = new ViewHolder(view);
                        return viewHolder;
                    }
                };

        mRecyclerView.setAdapter(mAdapter);
        mAdapter.startListening();
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        mFabSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String getSearchText=mEdtComment.getText().toString();
                AddComment(getSearchText);
            }
        });


    }
    //class of view holder for recyclerview to show items
    private static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name, comment;
        private CircleImageView image;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.layoutcommentusername);
            comment = (TextView) itemView.findViewById(R.id.layoutcommentusercomment);
            image = (CircleImageView) itemView.findViewById(R.id.layoutcommentuserimage);


        }
    }

    private void AddComment(String getSearchText) {
        ShowDialog("Por favor espere el comentario agregar ...");
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put("userid",mCurrentUserId);
        hashMap.put("usercomment",getSearchText);
        String pushId=mRootRef.child("Comments").child(getPostId).push().getKey();
        mRootRef.child("Comments").child(getPostId).child(pushId).setValue(hashMap)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            HideDialog();
                            mEdtComment.setText("");
                            Toast.makeText(CommentActivity.this, "Comentario Agregar exitosamente", Toast.LENGTH_SHORT).show();
                        } else {
                            HideDialog();
                            Toast.makeText(CommentActivity.this, "", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void ShowDialog(String message) {
        if (mLoadingBar != null && !mLoadingBar.isShowing()) {
            mLoadingBar.setCancelable(true);
            mLoadingBar.setMessage(message);
            mLoadingBar.show();
        }
    }

    private void HideDialog() {
        if (mLoadingBar != null && mLoadingBar.isShowing()) {
            mLoadingBar.dismiss();
        }
    }

}
package com.mtechsolution.simplesocialmedia.Utils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;


public class BaseApp extends Application {

    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private String mCurrentUserId;
    private DatabaseReference mRootRef;
    private SharedPreferences mSharePref;

    @Override
    public void onCreate() {
        super.onCreate();

//        FirebaseApp.initializeApp(getApplicationContext());
        //for text
//        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
//
//        //for images
//        Picasso.Builder builder = new Picasso.Builder(this);
//        builder.downloader(new OkHttpDownloader(this, Integer.MAX_VALUE));
//        Picasso built = builder.build();
//        built.setIndicatorsEnabled(false);
//        built.setLoggingEnabled(true);
//        Picasso.setSingletonInstance(built);

        //
        mAuth=FirebaseAuth.getInstance();
        mCurrentUser=mAuth.getCurrentUser();
        mRootRef=FirebaseDatabase.getInstance().getReference().child("Users");
        mSharePref = getSharedPreferences("socialmediaapp", MODE_PRIVATE);

        FirebaseMessaging.getInstance().subscribeToTopic("MiNegocioLPZ");

        if (FirebaseAuth.getInstance().getCurrentUser() != null){
            FirebaseMessaging.getInstance().subscribeToTopic(FirebaseAuth.getInstance().getCurrentUser().getUid());
            mCurrentUserId=mCurrentUser.getUid();
        }

        registerActivityLifecycleCallbacks(new AppLifecycleTracker());
    }
    public class AppLifecycleTracker implements ActivityLifecycleCallbacks {

        @Override
        public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {

        }

        @Override
        public void onActivityStarted(@NonNull Activity activity) {
            if (mCurrentUser!=null){
                if (activity.getLocalClassName().equals("Activity.MessageActivity")){
                    Map hashMap=new HashMap();
                    hashMap.put("online","true");
                    hashMap.put("notification",mSharePref.getString("receiverid","null"));
                    mRootRef.child(mCurrentUserId).updateChildren(hashMap);
                } else {
                    mRootRef.child(mCurrentUserId).child("online").setValue("true");
                }

            }


        }

        @Override
        public void onActivityResumed(@NonNull Activity activity) {

        }

        @Override
        public void onActivityPaused(@NonNull Activity activity) {

        }

        @Override
        public void onActivityStopped(@NonNull Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(@NonNull Activity activity) {
            if (mCurrentUser!=null) {
                if (!activity.getLocalClassName().equals("Activity.LoginSignUpActivity") ||
                        !activity.getLocalClassName().equals("Activity.StarterInfoActivity") ) {
                    mCurrentUserId = mCurrentUser.getUid();
                    Map hashMap = new HashMap();
                    hashMap.put("online", "false");
                    hashMap.put("notification", "false");
                    mRootRef.child(mCurrentUserId).updateChildren(hashMap);
                }
            }

        }
    }
}

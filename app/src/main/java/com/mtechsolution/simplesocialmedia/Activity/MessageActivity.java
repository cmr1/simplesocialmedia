package com.mtechsolution.simplesocialmedia.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mtechsolution.simplesocialmedia.Adapter.MessageAdapter;
import com.mtechsolution.simplesocialmedia.Model.MessagesModel;
import com.mtechsolution.simplesocialmedia.R;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessageActivity extends AppCompatActivity {

    private ImageView mBack;
    private String getImage,getName,getUserId;
    private TextView mTextName,mTextOnlineStatus;
    private CircleImageView mImage;
    private FloatingActionButton mFabSendMessage;
    private EditText mEdtTextMessage;
    private RecyclerView mRecyclewView;
    private SharedPreferences mSharePref;
    private String mCurrentUserId;
    private FirebaseUser mCurrentUser;
    private FirebaseAuth mAuth;
    private DatabaseReference mRootRef, mOnlineUserReference;
    private String mSaveCurrentTime, mSaveCurrentDate;
    private Toast mToast;
    private ProgressDialog mLoadingBar;


    private List<MessagesModel> messagesList = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private MessageAdapter messageAdapter;

    //for notification
    private RequestQueue mRequestQue;
    private String URL = "https://fcm.googleapis.com/fcm/send";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        mRequestQue = Volley.newRequestQueue(this);

        getImage=getIntent().getExtras().getString("image");
        getName=getIntent().getExtras().getString("name");
        getUserId=getIntent().getExtras().getString("userid");

        mBack=(ImageView) findViewById(R.id.back);
        mTextName=(TextView) findViewById(R.id.textmessageusername);
        mTextOnlineStatus=(TextView) findViewById(R.id.textmessageonlinestatus);
        mImage=(CircleImageView) findViewById(R.id.textmessageuserimage);
        mEdtTextMessage=(EditText) findViewById(R.id.edtmessage);
        mFabSendMessage=(FloatingActionButton) findViewById(R.id.fabmessagesend);
        mRecyclewView=(RecyclerView) findViewById(R.id.recyclerviewmessages);

        Picasso.with(this).load(getImage).placeholder(R.drawable.ic_user).into(mImage);
        mTextName.setText(getName);

        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();
        mCurrentUserId = mCurrentUser.getUid();
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mOnlineUserReference = FirebaseDatabase.getInstance().getReference().child("Users");
        mSharePref = getSharedPreferences("socialmediaapp", MODE_PRIVATE);
        mLoadingBar = new ProgressDialog(this);

        // For notification
        SharedPreferences.Editor editor=mSharePref.edit();
        editor.putString("receiverid",getUserId);
        editor.apply();

        //For clear unseen messsage
        mRootRef.child("Unseen").child("Message").child(mCurrentUserId).child(getUserId).removeValue();

        messageAdapter = new MessageAdapter(messagesList, this);
        linearLayoutManager = new LinearLayoutManager(this);
        mRecyclewView.setLayoutManager(linearLayoutManager);
        mRecyclewView.setAdapter(messageAdapter);

        registerForContextMenu(mRecyclewView);

        //Check online status
        mOnlineUserReference.child(getUserId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    String getonlinestatus=dataSnapshot.child("online").getValue().toString();

                    if (getonlinestatus.equals("true")){
                        mTextOnlineStatus.setText("En línea");
                    } else {
                        mTextOnlineStatus.setText("Desconectada");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mFabSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String gettext=mEdtTextMessage.getText().toString();
                if (gettext.isEmpty()){

                } else {
                    if (isOnline()) {
                        SendMessage(gettext);
                        mEdtTextMessage.setText("");
                    } else {
                        Toast.makeText(MessageActivity.this, "Sin conexión a Internet", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.chatactivitymenu, menu);
        return true;
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                messagesList.remove(messageAdapter.getPositions());
                messageAdapter.notifyDataSetChanged();

                mRootRef.child("Message").child(getUserId).child(mCurrentUserId).child(messageAdapter.getPushIdPositions()).removeValue();
                mRootRef.child("Message").child(mCurrentUserId).child(getUserId).child(messageAdapter.getPushIdPositions()).removeValue();
                break;
            case 1:
                messagesList.remove(messageAdapter.getPositions());
                messageAdapter.notifyDataSetChanged();

                mRootRef.child("Message").child(mCurrentUserId).child(getUserId).child(messageAdapter.getPushIdPositions()).removeValue();
                break;
        }
        return super.onContextItemSelected(item);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.chatactivityclearchat:
                ShowDialog("Espere, mensaje eliminado...");
                messagesList.clear();
                messageAdapter.notifyDataSetChanged();
                mRootRef.child("Message").child(mCurrentUserId).child(getUserId).setValue(null)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    HideDialog();
                                } else {
                                    ShowToast("Mensaje de error eliminado...", Toast.LENGTH_SHORT, Gravity.BOTTOM);
                                    HideDialog();
                                }
                            }
                        });


                break;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onStart() {
        super.onStart();
        //for messages
        mRootRef.child("Message").child(mCurrentUserId).child(getUserId)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        MessagesModel messages = dataSnapshot.getValue(MessagesModel.class);
                        messagesList.add(messages);
                        messageAdapter.notifyDataSetChanged();

                        mRecyclewView.smoothScrollToPosition(mRecyclewView.getAdapter().getItemCount());
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }
    private void SendNotification(String title,String description) {

        JSONObject json = new JSONObject();
        try {
            json.put("to","/topics/"+getUserId);
            JSONObject notificationObj = new JSONObject();
            notificationObj.put("title",title);
            notificationObj.put("body",description);
            json.put("notification",notificationObj);

            JSONObject dataObj = new JSONObject();
            dataObj.put("sender_id",mCurrentUserId);
            dataObj.put("sender_name",mSharePref.getString("name","null"));
            dataObj.put("sender_pic",mSharePref.getString("image","null"));
            json.put("data",dataObj);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,
                    json,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Log.d("MUR", "onResponse: ");

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("MUR", "onError: "+error.networkResponse);

                }
            }
            ){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> header = new HashMap<>();
                    header.put("content-type","application/json");
                    header.put("authorization","key=AAAAJOA0nbE:APA91bF2_UI7mJUU-4YFd3OC3ste5HQhRZMC7UNUXyMoAVx_0DjES_WslD6Zehp_NCvpArJbvAefEaYT5FNHdkm2OGohTmIvt_oykfdSep1quTpskff1zq5UbLaogCKixlgL0MIpLXKx");
                    return header;
                }
            };
            mRequestQue.add(request);
        }
        catch (JSONException e)

        {
            e.printStackTrace();
        }
    }
    private void SendMessage(final String message) {

        String messagesenderref = "Message/" + mCurrentUserId + "/" + getUserId;
        final String messagereceiverref = "Message/" + getUserId + "/" + mCurrentUserId;

        DatabaseReference usermessagekeyref = mRootRef.child("Message")
                .child(mCurrentUserId).child(getUserId).push();
        final String messagepushid = usermessagekeyref.getKey();

        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat currentDate = new SimpleDateFormat("MMM dd, yyyy");
        mSaveCurrentDate = currentDate.format(calendar.getTime());

        SimpleDateFormat currentTime = new SimpleDateFormat("hh:mm a");
        mSaveCurrentTime = currentTime.format(calendar.getTime());

        Map messagetextbody = new HashMap();
        messagetextbody.put("message", message);
        messagetextbody.put("type", "text");
        messagetextbody.put("from", mCurrentUserId);
        messagetextbody.put("to", getUserId);
        messagetextbody.put("date", mSaveCurrentDate);
        messagetextbody.put("time", mSaveCurrentTime);
        messagetextbody.put("messagepushid", messagepushid);

        Map messagebodydetail = new HashMap();
        messagebodydetail.put(messagesenderref + "/" + messagepushid, messagetextbody);
        messagebodydetail.put(messagereceiverref + "/" + messagepushid, messagetextbody);

        mRootRef.updateChildren(messagebodydetail).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()) {
                    //Check notification status
                    mOnlineUserReference.child(getUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()){
                                String getnotification=dataSnapshot.child("notification").getValue().toString();

                                if (getnotification.equals(mCurrentUserId)){

                                } else {
                                    mRootRef.child("Unseen").child(messagereceiverref).child(messagepushid).child(messagepushid).setValue(messagepushid);
                                    //send notification when message send successful
                                    SendNotification(getResources().getString(R.string.app_name),message);
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                } else {
                    ShowToast("Error de envío de mensaje...", Toast.LENGTH_SHORT, Gravity.BOTTOM);
                }

            }
        });

    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    private void ShowToast(String message, int length, int gravity) {
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(MessageActivity.this, message, length);
        mToast.setGravity(gravity, 0, 0);
        mToast.show();
    }

    private void ShowDialog(String message) {
        if (mLoadingBar != null && !mLoadingBar.isShowing()) {
            mLoadingBar.setCancelable(false);
            mLoadingBar.setMessage(message);
            mLoadingBar.show();
        }
    }

    private void HideDialog() {
        if (mLoadingBar != null && mLoadingBar.isShowing()) {
            mLoadingBar.dismiss();
        }
    }
}

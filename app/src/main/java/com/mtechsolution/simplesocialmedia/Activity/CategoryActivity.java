package com.mtechsolution.simplesocialmedia.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mtechsolution.simplesocialmedia.Fragment.HomeFragment;
import com.mtechsolution.simplesocialmedia.Model.PostModel;
import com.mtechsolution.simplesocialmedia.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class CategoryActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private String mCurrentUserId;
    private RecyclerView mRecyclerViewSimple,mRecyclerViewPremium;
    private TextView mTextNoItemFound;
    private DatabaseReference mRootRef;
    private FirebaseRecyclerAdapter<PostModel, ViewHolder> mAdapterPremium;
    private FirebaseRecyclerAdapter<PostModel, ViewHolder> mAdapterSimple;
    private ProgressDialog mLoadingBar;
    private String getCategoryName;
    private boolean isPremiumNoItemFound=false;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        getCategoryName=getIntent().getExtras().getString("categoryname");

        mRecyclerViewPremium = (RecyclerView) findViewById(R.id.recyclerviewcategorypremium);
        mRecyclerViewSimple = (RecyclerView) findViewById(R.id.recyclerviewcategorysimple);
        mTextNoItemFound = (TextView) findViewById(R.id.textnoitemfound);
        mToolbar=(Toolbar) findViewById(R.id.toolbar);


        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getCategoryName);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mAuth= FirebaseAuth.getInstance();
        mCurrentUser=mAuth.getCurrentUser();
        mCurrentUserId = mCurrentUser.getUid();
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mLoadingBar = new ProgressDialog(this);



        //For Premium Posts
        /////////////////////////////////////////////////////////////////////////////////////////////
        mRecyclerViewPremium.setHasFixedSize(true);
        //set layout manager to recycler view
        mRecyclerViewPremium.setLayoutManager(new LinearLayoutManager(this));

        Query queriesPremium = mRootRef.child("Posts").child("Premium").orderByChild("category").equalTo(getCategoryName);
        FirebaseRecyclerOptions<PostModel> optionsPremium =
                new FirebaseRecyclerOptions.Builder<PostModel>()
                        .setQuery(queriesPremium, PostModel.class)
                        .build();

        mAdapterPremium =
                new FirebaseRecyclerAdapter<PostModel, ViewHolder>(optionsPremium) {
                    @Override
                    protected void onBindViewHolder(@NonNull final ViewHolder holder, final int position, @NonNull final PostModel model) {


                        holder.title.setText(model.getTitle());
                        holder.description.setText(model.getDescription());
                        holder.compaignurl.setText(model.getCompaignurl());
                        mRootRef.child("Users").child(model.getUserid())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            String username=dataSnapshot.child("name").getValue().toString();
                                            String userimage=dataSnapshot.child("image").getValue().toString();
                                            holder.username.setText(username);
                                            Picasso.with(CategoryActivity.this).load(userimage).placeholder(R.drawable.ic_user).into(holder.userimage);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                        Picasso.with(CategoryActivity.this).load(model.getPostimage()).placeholder(R.drawable.ic_image).into(holder.postimage);

                        mRootRef.child("Comments").child(getRef(position).getKey())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            long totalChild=dataSnapshot.getChildrenCount();
                                            holder.commentnumber.setText(""+totalChild);
                                        } else {
                                            holder.commentnumber.setText("0");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                        mRootRef.child("isFavorite").child(getRef(position).getKey())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            long totalChild=dataSnapshot.getChildrenCount();
                                            holder.favoritenumber.setText(""+totalChild);
                                        } else {
                                            holder.favoritenumber.setText("0");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                        mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()){
                                    holder.imagefavorite.setColorFilter(ContextCompat.getColor(CategoryActivity.this, R.color.red));
                                } else {
                                    holder.imagefavorite.setColorFilter(ContextCompat.getColor(CategoryActivity.this, R.color.black));
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        holder.mLayoutComment.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent=new Intent(CategoryActivity.this, CommentActivity.class);
                                intent.putExtra("postid",getRef(position).getKey());
                                intent.putExtra("posttitle",model.getTitle());
                                startActivity(intent);
                            }
                        });
                        holder.imagefavorite.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            int getTotalFavorite= Integer.parseInt(holder.favoritenumber.getText().toString());
                                            holder.favoritenumber.setText(String.valueOf(getTotalFavorite-1));
                                            holder.imagefavorite.setColorFilter(ContextCompat.getColor(CategoryActivity.this, R.color.black));
                                            mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).removeValue();
                                        } else {
                                            int getTotalFavorite= Integer.parseInt(holder.favoritenumber.getText().toString());
                                            holder.favoritenumber.setText(String.valueOf(getTotalFavorite+1));
                                            holder.imagefavorite.setColorFilter(ContextCompat.getColor(CategoryActivity.this, R.color.red));
                                            mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).child("favorite").setValue("yes");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                        });

                        holder.compaignurl.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent browserIntent = new Intent(CategoryActivity.this, WebViewActivity.class);
                                browserIntent.putExtra("url",model.getCompaignurl());
                                startActivity(browserIntent);
                            }
                        });
                    }

                    @NonNull
                    @Override
                    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                        View view = LayoutInflater.from(viewGroup.getContext())
                                .inflate(R.layout.layoutpremiumpost, viewGroup, false);
                        ViewHolder viewHolder = new ViewHolder(view);
                        return viewHolder;
                    }
                };

        mRecyclerViewPremium.setAdapter(mAdapterPremium);
        mAdapterPremium.startListening();

        queriesPremium.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    mTextNoItemFound.setVisibility(View.GONE);
                    mRecyclerViewPremium.setVisibility(View.VISIBLE);
                }
                else{
                    mTextNoItemFound.setVisibility(View.VISIBLE);
                    isPremiumNoItemFound=true;
                    mRecyclerViewPremium.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //For Simple Posts
        /////////////////////////////////////////////////////////////////////////////////////////////
        mRecyclerViewSimple.setHasFixedSize(true);
        //set layout manager to recycler view
        mRecyclerViewSimple.setLayoutManager(new LinearLayoutManager(this));

        Query queriesSimple = mRootRef.child("Posts").child("Simple").orderByChild("category").equalTo(getCategoryName);
        FirebaseRecyclerOptions<PostModel> optionsSimple =
                new FirebaseRecyclerOptions.Builder<PostModel>()
                        .setQuery(queriesSimple, PostModel.class)
                        .build();

        mAdapterSimple =
                new FirebaseRecyclerAdapter<PostModel, ViewHolder>(optionsSimple) {
                    @Override
                    protected void onBindViewHolder(@NonNull final ViewHolder holder, final int position, @NonNull final PostModel model) {


                        holder.title.setText(model.getTitle());
                        holder.description.setText(model.getDescription());
                        holder.compaignurl.setText(model.getCompaignurl());
                        mRootRef.child("Users").child(model.getUserid())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            String username=dataSnapshot.child("name").getValue().toString();
                                            String userimage=dataSnapshot.child("image").getValue().toString();
                                            holder.username.setText(username);
                                            Picasso.with(CategoryActivity.this).load(userimage).placeholder(R.drawable.ic_user).into(holder.userimage);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                        Picasso.with(CategoryActivity.this).load(model.getPostimage()).placeholder(R.drawable.ic_image).into(holder.postimage);

                        mRootRef.child("Comments").child(getRef(position).getKey())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            long totalChild=dataSnapshot.getChildrenCount();
                                            holder.commentnumber.setText(""+totalChild);
                                        } else {
                                            holder.commentnumber.setText("0");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                        mRootRef.child("isFavorite").child(getRef(position).getKey())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            long totalChild=dataSnapshot.getChildrenCount();
                                            holder.favoritenumber.setText(""+totalChild);
                                        } else {
                                            holder.favoritenumber.setText("0");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                        mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()){
                                    holder.imagefavorite.setColorFilter(ContextCompat.getColor(CategoryActivity.this, R.color.red));
                                } else {
                                    holder.imagefavorite.setColorFilter(ContextCompat.getColor(CategoryActivity.this, R.color.black));
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        holder.mLayoutComment.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent=new Intent(CategoryActivity.this, CommentActivity.class);
                                intent.putExtra("postid",getRef(position).getKey());
                                intent.putExtra("posttitle",model.getTitle());
                                startActivity(intent);
                            }
                        });
                        holder.imagefavorite.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            int getTotalFavorite= Integer.parseInt(holder.favoritenumber.getText().toString());
                                            holder.favoritenumber.setText(String.valueOf(getTotalFavorite-1));
                                            holder.imagefavorite.setColorFilter(ContextCompat.getColor(CategoryActivity.this, R.color.black));
                                            mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).removeValue();
                                        } else {
                                            int getTotalFavorite= Integer.parseInt(holder.favoritenumber.getText().toString());
                                            holder.favoritenumber.setText(String.valueOf(getTotalFavorite+1));
                                            holder.imagefavorite.setColorFilter(ContextCompat.getColor(CategoryActivity.this, R.color.red));
                                            mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).child("favorite").setValue("yes");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                        });

                        holder.compaignurl.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent browserIntent = new Intent(CategoryActivity.this, WebViewActivity.class);
                                browserIntent.putExtra("url",model.getCompaignurl());
                                startActivity(browserIntent);
                            }
                        });
                    }

                    @NonNull
                    @Override
                    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                        View view = LayoutInflater.from(viewGroup.getContext())
                                .inflate(R.layout.layoutpost, viewGroup, false);
                        ViewHolder viewHolder = new ViewHolder(view);
                        return viewHolder;
                    }
                };

        mRecyclerViewSimple.setAdapter(mAdapterSimple);
        mAdapterSimple.startListening();

        if (isPremiumNoItemFound) {
            queriesSimple.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        mTextNoItemFound.setVisibility(View.GONE);
                        mRecyclerViewSimple.setVisibility(View.VISIBLE);
                    } else {
                        mTextNoItemFound.setVisibility(View.VISIBLE);
                        mRecyclerViewSimple.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }
    //class of view holder for recyclerview to show items
    private static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title, description,username,compaignurl,favoritenumber,commentnumber;
        private CircleImageView userimage;
        private ImageView postimage,imagefavorite;
        private LinearLayout mLayoutFavorite,mLayoutComment;

        public ViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.layoutposttitle);
            description = (TextView) itemView.findViewById(R.id.layoutpostdescription);
            username = (TextView) itemView.findViewById(R.id.layoutusername);
            compaignurl = (TextView) itemView.findViewById(R.id.layoutpostcompaignurl);
            favoritenumber = (TextView) itemView.findViewById(R.id.layoutpostfavoritenumber);
            commentnumber = (TextView) itemView.findViewById(R.id.layoutpostcommentnumber);
            userimage = (CircleImageView) itemView.findViewById(R.id.layoutuserimage);
            postimage = (ImageView) itemView.findViewById(R.id.layoutpostimage);
            imagefavorite = (ImageView) itemView.findViewById(R.id.imagefavorite);
            mLayoutFavorite = (LinearLayout) itemView.findViewById(R.id.layoutfavorite);
            mLayoutComment = (LinearLayout) itemView.findViewById(R.id.layoutcomment);


        }
    }
    private void ShowDialog(String message) {
        if (mLoadingBar != null && !mLoadingBar.isShowing()) {
            mLoadingBar.setCancelable(true);
            mLoadingBar.setMessage(message);
            mLoadingBar.show();
        }
    }

    private void HideDialog() {
        if (mLoadingBar != null && mLoadingBar.isShowing()) {
            mLoadingBar.dismiss();
        }
    }
}
package com.mtechsolution.simplesocialmedia.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mtechsolution.simplesocialmedia.Activity.AllUserActivity;
import com.mtechsolution.simplesocialmedia.Activity.MessageActivity;
import com.mtechsolution.simplesocialmedia.Model.MessagesModel;
import com.mtechsolution.simplesocialmedia.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessageFragment extends Fragment {

    private FloatingActionButton mFabAddUser;
    private RecyclerView mRecyclerView;
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private DatabaseReference mRootRef;
    private String mCurrentUserId;
    private FirebaseRecyclerAdapter<MessagesModel, ViewHolder> mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_message, container, false);

        mRecyclerView = root.findViewById(R.id.recyclerviewchatlist);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setHasFixedSize(true);

        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();
        mCurrentUserId = mCurrentUser.getUid();
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mRootRef.keepSynced(true);


        mFabAddUser=(FloatingActionButton) root.findViewById(R.id.fabadduser);
        mFabAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(), AllUserActivity.class);
                startActivity(intent);
            }
        });

        return root;
    }
    //class of view holder for recyclerview to show items
    private static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView name,lastmessage,lastmessagetime,totalunseenmessage;
        private CircleImageView image;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.layoutchatusername);
            totalunseenmessage = (TextView) itemView.findViewById(R.id.layoutchatuserunseenmessage);
            lastmessagetime = (TextView) itemView.findViewById(R.id.layouchatlastmessagetime);
            lastmessage = (TextView) itemView.findViewById(R.id.layoutchatuserlastmessage);
            image = (CircleImageView) itemView.findViewById(R.id.layouchatimage);

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseRecyclerOptions<MessagesModel> options =
                new FirebaseRecyclerOptions.Builder<MessagesModel>()
                        .setQuery(mRootRef.child("Message").child(mCurrentUserId), MessagesModel.class)
                        .build();

        mAdapter =
                new FirebaseRecyclerAdapter<MessagesModel, ViewHolder>(options) {
                    @Override
                    protected void onBindViewHolder(@NonNull final ViewHolder holder, final int position, @NonNull final MessagesModel model) {
                        mRootRef.child("Unseen").child("Message").child(mCurrentUserId).child(getRef(position).getKey())
                                .addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()) {
                                            // get total available quest
                                            int size = (int) dataSnapshot.getChildrenCount();
                                            holder.totalunseenmessage.setText(String.valueOf(size));
                                            holder.totalunseenmessage.setVisibility(View.VISIBLE);

                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                        mRootRef.child("Users").child(getRef(position).getKey())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()) {
                                            String name = dataSnapshot.child("name").getValue().toString();
                                            final String pic = dataSnapshot.child("image").getValue().toString();

                                            holder.name.setText(name);
                                            Picasso.with(getContext()).load(pic).placeholder(R.drawable.ic_user)
                                                    .into(holder.image);


                                        }

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                        //Get Last Message and date time
                        mRootRef.child("Message").child(mCurrentUserId).child(getRef(position).getKey())
                                .addChildEventListener(new ChildEventListener() {
                                    @Override
                                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                        if (dataSnapshot.exists()) {
                                            String lastmessage = dataSnapshot.child("message").getValue().toString();
                                            String time = dataSnapshot.child("time").getValue().toString();
                                            holder.lastmessage.setText(lastmessage);
                                            holder.lastmessagetime.setText(time);
                                        }
                                    }

                                    @Override
                                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                                    }

                                    @Override
                                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });


                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mRootRef.child("Users").child(getRef(position).getKey())
                                        .addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                if (dataSnapshot.exists()) {

                                                    String name = dataSnapshot.child("name").getValue().toString();
                                                    final String pic = dataSnapshot.child("image").getValue().toString();


                                                    Intent intent = new Intent(getContext(), MessageActivity.class);
                                                    intent.putExtra("name", name);
                                                    intent.putExtra("image", pic);
                                                    intent.putExtra("userid", getRef(position).getKey());
                                                    startActivity(intent);
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });
                            }
                        });
                    }

                    @NonNull
                    @Override
                    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                        View view = LayoutInflater.from(viewGroup.getContext())
                                .inflate(R.layout.layoutchat, viewGroup, false);
                        ViewHolder viewHolder = new ViewHolder(view);
                        return viewHolder;
                    }
                };

        mRecyclerView.setAdapter(mAdapter);
        mAdapter.startListening();
    }
}
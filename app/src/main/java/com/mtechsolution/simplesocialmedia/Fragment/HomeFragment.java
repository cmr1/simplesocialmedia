package com.mtechsolution.simplesocialmedia.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mtechsolution.simplesocialmedia.Activity.CategoryActivity;
import com.mtechsolution.simplesocialmedia.Activity.CommentActivity;
import com.mtechsolution.simplesocialmedia.Activity.MyPostsActivity;
import com.mtechsolution.simplesocialmedia.Activity.UserProfileActivity;
import com.mtechsolution.simplesocialmedia.Activity.WebViewActivity;
import com.mtechsolution.simplesocialmedia.Model.PostModel;
import com.mtechsolution.simplesocialmedia.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;


public class HomeFragment extends Fragment {

    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private String mCurrentUserId;
    private RecyclerView mRecyclerViewSimple,mRecyclerViewPremium;
    private DatabaseReference mRootRef;
    private FirebaseRecyclerAdapter<PostModel, ViewHolder> mAdapterSimple;
    private FirebaseRecyclerAdapter<PostModel, ViewHolder> mAdapterPremium;
    private ProgressDialog mLoadingBar;
    private CircleImageView mImageRestaurants,mImageShopping,mImageServices,mImageSocial,mImageFitness,mImageCulture,
            mImageHealth,mImageAutomobile,mImageBeauty,mImagePets,mImageConstruction,mImageEducation,mImageSports,
    mImageHouse,mImageDigital,mImageTourism,mImageHotDogs,mImageOther;

    public View onCreateView(@NonNull final LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);

        mRecyclerViewPremium = (RecyclerView) root.findViewById(R.id.recyclerviewpostpremium);
        mRecyclerViewSimple = (RecyclerView) root.findViewById(R.id.recyclerviewpostsimple);
        mImageRestaurants=(CircleImageView) root.findViewById(R.id.imageresturant);
        mImageShopping=(CircleImageView) root.findViewById(R.id.imageshopping);
        mImageServices=(CircleImageView) root.findViewById(R.id.imageservices);
        mImageSocial=(CircleImageView) root.findViewById(R.id.imagesocial);
        mImageFitness=(CircleImageView) root.findViewById(R.id.imagefitness);
        mImageCulture=(CircleImageView) root.findViewById(R.id.imageculture);
        mImageHealth=(CircleImageView) root.findViewById(R.id.imagehealth);
        mImageAutomobile=(CircleImageView) root.findViewById(R.id.imageautomobile);
        mImageBeauty=(CircleImageView) root.findViewById(R.id.imagebeauty);
        mImagePets=(CircleImageView) root.findViewById(R.id.imagepets);
        mImageConstruction=(CircleImageView) root.findViewById(R.id.imageconstruction);
        mImageEducation=(CircleImageView) root.findViewById(R.id.imageeducation);
        mImageSports=(CircleImageView) root.findViewById(R.id.imagesports);
        mImageHouse=(CircleImageView) root.findViewById(R.id.imagehouse);
        mImageDigital=(CircleImageView) root.findViewById(R.id.imagedigital);
        mImageTourism=(CircleImageView) root.findViewById(R.id.imagetourism);
        mImageHotDogs=(CircleImageView) root.findViewById(R.id.imagehotdog);
        mImageOther=(CircleImageView) root.findViewById(R.id.imageother);



        mAuth=FirebaseAuth.getInstance();
        mCurrentUser=mAuth.getCurrentUser();
        if (mCurrentUser != null) {
            mCurrentUserId = mCurrentUser.getUid();
        }
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mLoadingBar = new ProgressDialog(getContext());


        //For Premium Posts
        /////////////////////////////////////////////////////////////////////////////////////////////
        mRecyclerViewPremium.setHasFixedSize(true);
        //set layout manager to recycler view
        mRecyclerViewPremium.setLayoutManager(new LinearLayoutManager(getContext()));

        FirebaseRecyclerOptions<PostModel> optionsPremium =
                new FirebaseRecyclerOptions.Builder<PostModel>()
                        .setQuery(mRootRef.child("Posts").child("Premium"), PostModel.class)
                        .build();

        mAdapterPremium =
                new FirebaseRecyclerAdapter<PostModel, ViewHolder>(optionsPremium) {
                    @Override
                    protected void onBindViewHolder(@NonNull final ViewHolder holder, final int position, @NonNull final PostModel model) {

                        holder.title.setText(model.getTitle());
                        holder.description.setText(model.getDescription());
                        holder.compaignurl.setText(model.getCompaignurl());

                        mRootRef.child("Users").child(model.getUserid())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()){
                                    String username=dataSnapshot.child("name").getValue().toString();
                                    String userimage=dataSnapshot.child("image").getValue().toString();
                                    holder.username.setText(username);
                                    Picasso.with(getContext()).load(userimage).placeholder(R.drawable.ic_user).into(holder.userimage);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        Picasso.with(getContext()).load(model.getPostimage()).placeholder(R.drawable.ic_image).into(holder.postimage);

                        mRootRef.child("Comments").child(getRef(position).getKey())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            long totalChild=dataSnapshot.getChildrenCount();
                                            holder.commentnumber.setText(""+totalChild);
                                        } else {
                                            holder.commentnumber.setText("0");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                        mRootRef.child("isFavorite").child(getRef(position).getKey())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            long totalChild=dataSnapshot.getChildrenCount();
                                            holder.favoritenumber.setText(""+totalChild);
                                        } else {
                                            holder.favoritenumber.setText("0");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                        mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()){
                                    holder.imagefavorite.setColorFilter(ContextCompat.getColor(getContext(), R.color.red));
                                } else {
                                    holder.imagefavorite.setColorFilter(ContextCompat.getColor(getContext(), R.color.black));
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        holder.mLayoutComment.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent=new Intent(getContext(), CommentActivity.class);
                                intent.putExtra("postid",getRef(position).getKey());
                                intent.putExtra("posttitle",model.getTitle());
                                startActivity(intent);
                            }
                        });
                        holder.imagefavorite.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            int getTotalFavorite= Integer.parseInt(holder.favoritenumber.getText().toString());
                                            holder.favoritenumber.setText(String.valueOf(getTotalFavorite-1));
                                            holder.imagefavorite.setColorFilter(ContextCompat.getColor(getContext(), R.color.black));
                                            mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).removeValue();
                                        } else {
                                            int getTotalFavorite= Integer.parseInt(holder.favoritenumber.getText().toString());
                                            holder.favoritenumber.setText(String.valueOf(getTotalFavorite+1));
                                            holder.imagefavorite.setColorFilter(ContextCompat.getColor(getContext(), R.color.red));
                                            mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).child("favorite").setValue("yes");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                        });

                        holder.compaignurl.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent browserIntent = new Intent(getContext(), WebViewActivity.class);
                                browserIntent.putExtra("url",model.getCompaignurl());
                                startActivity(browserIntent);
                            }
                        });
                    }

                    @NonNull
                    @Override
                    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                        View view = LayoutInflater.from(viewGroup.getContext())
                                .inflate(R.layout.layoutpremiumpost, viewGroup, false);
                        ViewHolder viewHolder = new ViewHolder(view);
                        return viewHolder;
                    }
                };

        mRecyclerViewPremium.setAdapter(mAdapterPremium);
        mAdapterPremium.startListening();

        mAdapterPremium.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = mAdapterPremium.getItemCount();
                int lastVisiblePosition =
                        new LinearLayoutManager(getContext()).findLastCompletelyVisibleItemPosition();
                // If the recycler view is initially being loaded or the
                // user is at the bottom of the list, scroll to the bottom
                // of the list to show the newly added message.
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    mRecyclerViewPremium.scrollToPosition(positionStart);
                }
            }
        });

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //For Simple Posts
        /////////////////////////////////////////////////////////////////////////////////////////////
        mRecyclerViewSimple.setHasFixedSize(true);
        //set layout manager to recycler view
        mRecyclerViewSimple.setLayoutManager(new LinearLayoutManager(getContext()));

        FirebaseRecyclerOptions<PostModel> optionsSimple =
                new FirebaseRecyclerOptions.Builder<PostModel>()
                        .setQuery(mRootRef.child("Posts").child("Simple"), PostModel.class)
                        .build();

        mAdapterSimple =
                new FirebaseRecyclerAdapter<PostModel, ViewHolder>(optionsSimple) {
                    @Override
                    protected void onBindViewHolder(@NonNull final ViewHolder holder, final int position, @NonNull final PostModel model) {


                        holder.title.setText(model.getTitle());
                        holder.description.setText(model.getDescription());
                        holder.compaignurl.setText(model.getCompaignurl());
                        mRootRef.child("Users").child(model.getUserid())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            String username=dataSnapshot.child("name").getValue().toString();
                                            String userimage=dataSnapshot.child("image").getValue().toString();
                                            holder.username.setText(username);
                                            Picasso.with(getContext()).load(userimage).placeholder(R.drawable.ic_user).into(holder.userimage);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                        Picasso.with(getContext()).load(model.getPostimage()).placeholder(R.drawable.ic_image).into(holder.postimage);

                        mRootRef.child("Comments").child(getRef(position).getKey())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            long totalChild=dataSnapshot.getChildrenCount();
                                            holder.commentnumber.setText(""+totalChild);
                                        } else {
                                            holder.commentnumber.setText("0");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                        mRootRef.child("isFavorite").child(getRef(position).getKey())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            long totalChild=dataSnapshot.getChildrenCount();
                                            holder.favoritenumber.setText(""+totalChild);
                                        } else {
                                            holder.favoritenumber.setText("0");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                        mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()){
                                    holder.imagefavorite.setColorFilter(ContextCompat.getColor(getContext(), R.color.red));
                                } else {
                                    holder.imagefavorite.setColorFilter(ContextCompat.getColor(getContext(), R.color.black));
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        holder.mLayoutComment.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent=new Intent(getContext(), CommentActivity.class);
                                intent.putExtra("postid",getRef(position).getKey());
                                intent.putExtra("posttitle",model.getTitle());
                                startActivity(intent);
                            }
                        });
                        holder.imagefavorite.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            int getTotalFavorite= Integer.parseInt(holder.favoritenumber.getText().toString());
                                            holder.favoritenumber.setText(String.valueOf(getTotalFavorite-1));
                                            holder.imagefavorite.setColorFilter(ContextCompat.getColor(getContext(), R.color.black));
                                            mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).removeValue();
                                        } else {
                                            int getTotalFavorite= Integer.parseInt(holder.favoritenumber.getText().toString());
                                            holder.favoritenumber.setText(String.valueOf(getTotalFavorite+1));
                                            holder.imagefavorite.setColorFilter(ContextCompat.getColor(getContext(), R.color.red));
                                            mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).child("favorite").setValue("yes");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                        });

                        holder.compaignurl.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent browserIntent = new Intent(getContext(), WebViewActivity.class);
                                browserIntent.putExtra("url",model.getCompaignurl());
                                startActivity(browserIntent);
                            }
                        });
                    }

                    @NonNull
                    @Override
                    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                        View view = LayoutInflater.from(viewGroup.getContext())
                                .inflate(R.layout.layoutpost, viewGroup, false);
                        ViewHolder viewHolder = new ViewHolder(view);
                        return viewHolder;
                    }
                };

        mRecyclerViewSimple.setAdapter(mAdapterSimple);
        mAdapterSimple.startListening();


        mAdapterSimple.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = mAdapterSimple.getItemCount();
                int lastVisiblePosition =
                        new LinearLayoutManager(getContext()).findLastCompletelyVisibleItemPosition();
                // If the recycler view is initially being loaded or the
                // user is at the bottom of the list, scroll to the bottom
                // of the list to show the newly added message.
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    mRecyclerViewSimple.scrollToPosition(positionStart);
                }
            }
        });
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        mImageRestaurants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),CategoryActivity.class);
                intent.putExtra("categoryname","Restaurantes");
                startActivity(intent);
            }
        });
        mImageShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),CategoryActivity.class);
                intent.putExtra("categoryname","Shopping");
                startActivity(intent);
            }
        });
        mImageServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),CategoryActivity.class);
                intent.putExtra("categoryname","Servicios");
                startActivity(intent);
            }
        });
        mImageSocial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),CategoryActivity.class);
                intent.putExtra("categoryname","Social");
                startActivity(intent);
            }
        });
        mImageFitness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),CategoryActivity.class);
                intent.putExtra("categoryname","Fitness");
                startActivity(intent);
            }
        });
        mImageCulture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),CategoryActivity.class);
                intent.putExtra("categoryname","Cultura");
                startActivity(intent);
            }
        });
        mImageHealth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),CategoryActivity.class);
                intent.putExtra("categoryname","Salud");
                startActivity(intent);
            }
        });
        mImageAutomobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),CategoryActivity.class);
                intent.putExtra("categoryname","Automóvil");
                startActivity(intent);
            }
        });
        mImageBeauty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),CategoryActivity.class);
                intent.putExtra("categoryname","Belleza");
                startActivity(intent);
            }
        });
        mImagePets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),CategoryActivity.class);
                intent.putExtra("categoryname","Mascotas");
                startActivity(intent);
            }
        });
        mImageConstruction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),CategoryActivity.class);
                intent.putExtra("categoryname","Construcción");
                startActivity(intent);
            }
        });
        mImageEducation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),CategoryActivity.class);
                intent.putExtra("categoryname","Educación");
                startActivity(intent);
            }
        });
        mImageSports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),CategoryActivity.class);
                intent.putExtra("categoryname","Deportes");
                startActivity(intent);
            }
        });
        mImageHouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),CategoryActivity.class);
                intent.putExtra("categoryname","Casa");
                startActivity(intent);
            }
        });
        mImageDigital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),CategoryActivity.class);
                intent.putExtra("categoryname","Digital");
                startActivity(intent);
            }
        });
        mImageTourism.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),CategoryActivity.class);
                intent.putExtra("categoryname","Turismo");
                startActivity(intent);
            }
        });
        mImageHotDogs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),CategoryActivity.class);
                intent.putExtra("categoryname","Hates");
                startActivity(intent);
            }
        });
        mImageOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),CategoryActivity.class);
                intent.putExtra("categoryname","Otro");
                startActivity(intent);
            }
        });

        return root;
    }
    //class of view holder for recyclerview to show items
    private static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title, description,username,compaignurl,favoritenumber,commentnumber;
        private CircleImageView userimage;
        private ImageView postimage,imagefavorite;
        private LinearLayout mLayoutFavorite,mLayoutComment;

        public ViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.layoutposttitle);
            description = (TextView) itemView.findViewById(R.id.layoutpostdescription);
            username = (TextView) itemView.findViewById(R.id.layoutusername);
            compaignurl = (TextView) itemView.findViewById(R.id.layoutpostcompaignurl);
            favoritenumber = (TextView) itemView.findViewById(R.id.layoutpostfavoritenumber);
            commentnumber = (TextView) itemView.findViewById(R.id.layoutpostcommentnumber);
            userimage = (CircleImageView) itemView.findViewById(R.id.layoutuserimage);
            postimage = (ImageView) itemView.findViewById(R.id.layoutpostimage);
            imagefavorite = (ImageView) itemView.findViewById(R.id.imagefavorite);
            mLayoutFavorite = (LinearLayout) itemView.findViewById(R.id.layoutfavorite);
            mLayoutComment = (LinearLayout) itemView.findViewById(R.id.layoutcomment);


        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private void ShowDialog(String message) {
        if (mLoadingBar != null && !mLoadingBar.isShowing()) {
            mLoadingBar.setCancelable(true);
            mLoadingBar.setMessage(message);
            mLoadingBar.show();
        }
    }

    private void HideDialog() {
        if (mLoadingBar != null && mLoadingBar.isShowing()) {
            mLoadingBar.dismiss();
        }
    }
}
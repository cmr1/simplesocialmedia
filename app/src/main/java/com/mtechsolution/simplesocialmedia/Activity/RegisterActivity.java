package com.mtechsolution.simplesocialmedia.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mtechsolution.simplesocialmedia.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

public class RegisterActivity extends AppCompatActivity {

    private Button mBtnLogin,mBtnRegister;
    private EditText mEdtEmail,mEdtName,mEdtDOB,mEdtPassword,mEdtConfirmPassword;
    private RadioButton mRbMale,mRbFemale,mRbAgree;
    private Calendar myCalendar;
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private String mCurrentUserId;
    private DatabaseReference mRootRef;
    private ProgressDialog mLoadingBar;
    private SharedPreferences mSharePref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mEdtEmail=(EditText) findViewById(R.id.edtemail);
        mEdtName=(EditText) findViewById(R.id.edtname);
        mEdtDOB=(EditText) findViewById(R.id.edtdob);
        mEdtPassword=(EditText) findViewById(R.id.edtpassword);
        mEdtConfirmPassword=(EditText) findViewById(R.id.edtconfirmpassword);
        mRbMale=(RadioButton) findViewById(R.id.rbmale);
        mRbFemale=(RadioButton) findViewById(R.id.rbfemale);
        mRbAgree=(RadioButton) findViewById(R.id.rbagree);
        mBtnLogin=(Button) findViewById(R.id.btnlogin);
        mBtnRegister=(Button) findViewById(R.id.btnregister);

        myCalendar = Calendar.getInstance();
        mLoadingBar=new ProgressDialog(this);
        mSharePref = getSharedPreferences("socialmediaapp", MODE_PRIVATE);
        mAuth=FirebaseAuth.getInstance();


        mRbMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (mRbMale.isChecked()){
                    mRbFemale.setChecked(false);
                } else {
                    mRbFemale.setChecked(true);
                }
            }
        });
        mRbFemale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (mRbFemale.isChecked()){
                    mRbMale.setChecked(false);
                } else {
                    mRbMale.setChecked(true);
                }
            }
        });

        mBtnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String getname=mEdtName.getText().toString();
                String getemail=mEdtEmail.getText().toString();
                String getdob=mEdtDOB.getText().toString();
                String getpassword=mEdtPassword.getText().toString();
                String getconfirmpassword=mEdtConfirmPassword.getText().toString();
                if (getname.isEmpty()){
                    Toast.makeText(RegisterActivity.this, "Por favor ingrese el nombre", Toast.LENGTH_SHORT).show();
                } else if (getemail.isEmpty()){
                    Toast.makeText(RegisterActivity.this, "Por favor ingrese su correo electrónico", Toast.LENGTH_SHORT).show();
                } else if (getdob.isEmpty()){
                    Toast.makeText(RegisterActivity.this, "Por favor ingrese dob", Toast.LENGTH_SHORT).show();
                } else if (getpassword.isEmpty()){
                    Toast.makeText(RegisterActivity.this, "Por favor, ingrese contraseña", Toast.LENGTH_SHORT).show();
                } else if (getconfirmpassword.isEmpty()){
                    Toast.makeText(RegisterActivity.this, "Por favor ingrese confirmar contraseña", Toast.LENGTH_SHORT).show();
                } else if (!getpassword.equals(getconfirmpassword)){
                    Toast.makeText(RegisterActivity.this, "Acordar término y condición", Toast.LENGTH_SHORT).show();
                } else if (!mRbAgree.isChecked()){
                    Toast.makeText(RegisterActivity.this, "Acordar término y condición", Toast.LENGTH_SHORT).show();
                } else {

                    if (mRbFemale.isChecked()){
                        Register(getname,getemail,getdob,getpassword,"female");
                    } else if (mRbMale.isChecked()){
                        Register(getname,getemail,getdob,getpassword,"male");
                    } else {
                        Toast.makeText(RegisterActivity.this, "Por favor seleccione el género", Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });
        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(RegisterActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });


        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        mEdtDOB.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(RegisterActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }
    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        mEdtDOB.setText(sdf.format(myCalendar.getTime()));
    }
    private void Register(final String name,final String email, final String dob, final String password, final String gender){
        ShowDialog("Por favor espere registro de usuario...");
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            mCurrentUser = mAuth.getCurrentUser();
                            mCurrentUserId = mCurrentUser.getUid();
                            mRootRef= FirebaseDatabase.getInstance().getReference();
                            HashMap<String, String> add = new HashMap<>();
                            add.put("name", name);
                            add.put("email", email);
                            add.put("dob", dob);
                            add.put("password", password);
                            add.put("gender",gender);
                            add.put("online", "false");
                            add.put("notification", "false");
                            add.put("image", "false");
                            add.put("online", "false");
                            add.put("userid",mCurrentUserId);
                            mRootRef.child("Users").child(mCurrentUserId).setValue(add)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                SharedPreferences.Editor editor= mSharePref.edit();
                                                editor.putString("name", name);
                                                editor.putString("email", email);
                                                editor.putString("dob", dob);
                                                editor.putString("password", password);
                                                editor.putString("gender",gender);
                                                editor.putString("image","false");
                                                editor.putString("userid",mCurrentUserId);
                                                editor.apply();
                                                Intent intent=new Intent(RegisterActivity.this,MainActivity.class);
                                                startActivity(intent);
                                                finish();
                                                HideDialog();
                                            } else {
                                                Toast.makeText(RegisterActivity.this, "Database error", Toast.LENGTH_SHORT).show();
                                                HideDialog();
                                            }

                                        }
                                    });
                        } else {
                            Toast.makeText(getApplicationContext(), "¡Registro fallido! la cuenta ya existe", Toast.LENGTH_LONG).show();

                            HideDialog();
                        }

                    }

                });

    }
    private void ShowDialog(String message) {
        if (mLoadingBar != null && !mLoadingBar.isShowing()) {
            mLoadingBar.setCancelable(true);
            mLoadingBar.setMessage(message);
            mLoadingBar.show();
        }
    }

    private void HideDialog() {
        if (mLoadingBar != null && mLoadingBar.isShowing()) {
            mLoadingBar.dismiss();
        }
    }
}
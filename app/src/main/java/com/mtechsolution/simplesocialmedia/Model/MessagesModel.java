package com.mtechsolution.simplesocialmedia.Model;

/**
 * Created by FAROOQ on 2019-06-21.
 */

public class MessagesModel
{
    private String from, message, type, to, messageID, time, date, name,messagepushid,isseen;

    public MessagesModel()
    {

    }

    public MessagesModel(String from, String message, String type, String to, String messageID, String time, String date, String name, String messagepushid, String isseen) {
        this.from = from;
        this.message = message;
        this.type = type;
        this.to = to;
        this.messageID = messageID;
        this.time = time;
        this.date = date;
        this.name = name;
        this.messagepushid=messagepushid;
        this.isseen=isseen;
    }

    public String getMessagepushid() {
        return messagepushid;
    }

    public void setMessagepushid(String messagepushid) {
        this.messagepushid = messagepushid;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getMessageID() {
        return messageID;
    }

    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public String getIsseen() {
        return isseen;
    }

    public void setIsseen(String isseen) {
        this.isseen = isseen;
    }

    public void setName(String name) {
        this.name = name;
    }

}
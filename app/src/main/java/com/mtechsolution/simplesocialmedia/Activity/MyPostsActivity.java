package com.mtechsolution.simplesocialmedia.Activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mtechsolution.simplesocialmedia.Model.PostModel;
import com.mtechsolution.simplesocialmedia.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyPostsActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private EditText mEdtSearchText;
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private String mCurrentUserId;
    private RecyclerView mRecyclerViewPremium,mRecyclerViewSimple;
    private TextView mTextNoItemFound;
    private DatabaseReference mRootRef;
    private FirebaseRecyclerAdapter<PostModel, ViewHolder> mAdapterPremium;
    private FirebaseRecyclerAdapter<PostModel, ViewHolder> mAdapterSimple;
    private ProgressDialog mLoadingBar;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_posts);

        mToolbar=(Toolbar) findViewById(R.id.toolbar);
        mRecyclerViewPremium = (RecyclerView) findViewById(R.id.recyclerviewmypostpremium);
        mRecyclerViewSimple = (RecyclerView) findViewById(R.id.recyclerviewmypostsimple);
        mTextNoItemFound = (TextView) findViewById(R.id.textnoitemfound);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Mis Publicaciones");
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mAuth= FirebaseAuth.getInstance();
        mCurrentUser=mAuth.getCurrentUser();
        mCurrentUserId = mCurrentUser.getUid();
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mLoadingBar = new ProgressDialog(MyPostsActivity.this);


        mRecyclerViewPremium.setHasFixedSize(true);
        //set layout manager to recycler view
        mRecyclerViewPremium.setLayoutManager(new LinearLayoutManager(MyPostsActivity.this));

        mRecyclerViewSimple.setHasFixedSize(true);
        //set layout manager to recycler view
        mRecyclerViewSimple.setLayoutManager(new LinearLayoutManager(MyPostsActivity.this));

        PerformSearch(mCurrentUserId);

    }

    private void PerformSearch(String searchtext) {

        // For Premium Post
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        Query queriesPremium = mRootRef.child("Posts").child("Premium").orderByChild("userid").equalTo(searchtext);
        FirebaseRecyclerOptions<PostModel> optionsPremium =
                new FirebaseRecyclerOptions.Builder<PostModel>()
                        .setQuery(queriesPremium, PostModel.class)
                        .build();

        mAdapterPremium =
                new FirebaseRecyclerAdapter<PostModel, ViewHolder>(optionsPremium) {
                    @Override
                    protected void onBindViewHolder(@NonNull final ViewHolder holder, final int position, @NonNull final PostModel model) {

                        holder.title.setText(model.getTitle());
                        holder.description.setText(model.getDescription());
                        holder.compaignurl.setText(model.getCompaignurl());
                        mRootRef.child("Users").child(model.getUserid())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            String username=dataSnapshot.child("name").getValue().toString();
                                            String userimage=dataSnapshot.child("image").getValue().toString();
                                            holder.username.setText(username);
                                            Picasso.with(MyPostsActivity.this).load(userimage).placeholder(R.drawable.ic_user).into(holder.userimage);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                        Picasso.with(MyPostsActivity.this).load(model.getPostimage()).placeholder(R.drawable.ic_image).into(holder.postimage);

                        mRootRef.child("Comments").child(getRef(position).getKey())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            long totalChild=dataSnapshot.getChildrenCount();
                                            holder.commentnumber.setText(""+totalChild);
                                        } else {
                                            holder.commentnumber.setText("0");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                        mRootRef.child("isFavorite").child(getRef(position).getKey())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            long totalChild=dataSnapshot.getChildrenCount();
                                            holder.favoritenumber.setText(""+totalChild);
                                        } else {
                                            holder.favoritenumber.setText("0");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                        mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()){
                                    holder.imagefavorite.setColorFilter(ContextCompat.getColor(MyPostsActivity.this, R.color.red));
                                } else {
                                    holder.imagefavorite.setColorFilter(ContextCompat.getColor(MyPostsActivity.this, R.color.black));
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        holder.mLayoutComment.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent=new Intent(MyPostsActivity.this, CommentActivity.class);
                                intent.putExtra("postid",getRef(position).getKey());
                                intent.putExtra("posttitle",model.getTitle());
                                startActivity(intent);
                            }
                        });
                        holder.imagefavorite.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            int getTotalFavorite= Integer.parseInt(holder.favoritenumber.getText().toString());
                                            holder.favoritenumber.setText(String.valueOf(getTotalFavorite-1));
                                            holder.imagefavorite.setColorFilter(ContextCompat.getColor(MyPostsActivity.this, R.color.black));
                                            mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).removeValue();
                                        } else {
                                            int getTotalFavorite= Integer.parseInt(holder.favoritenumber.getText().toString());
                                            holder.favoritenumber.setText(String.valueOf(getTotalFavorite+1));
                                            holder.imagefavorite.setColorFilter(ContextCompat.getColor(MyPostsActivity.this, R.color.red));
                                            mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).child("favorite").setValue("yes");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                        });

                        holder.compaignurl.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent browserIntent = new Intent(MyPostsActivity.this, WebViewActivity.class);
                                browserIntent.putExtra("url",model.getCompaignurl());
                                startActivity(browserIntent);
                            }
                        });
                        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View view) {
                                AlertDialog alertDialog = new AlertDialog.Builder(MyPostsActivity.this).create();
                                alertDialog.setTitle("Alerta");
                                alertDialog.setMessage("¿Estás segura de eliminar el artículo?");
                                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "si",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                ShowDialog("Por favor espere eliminar el artículo...");
                                                mRootRef.child("Posts").child("Premium").child(getRef(position).getKey())
                                                        .removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()){
                                                            HideDialog();
                                                            Toast.makeText(MyPostsActivity.this, "Elemento eliminado correctamente", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            HideDialog();
                                                            Toast.makeText(MyPostsActivity.this, "Error", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                alertDialog.show();
                                return false;
                            }
                        });

                    }

                    @NonNull
                    @Override
                    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                        View view = LayoutInflater.from(viewGroup.getContext())
                                .inflate(R.layout.layoutpremiumpost, viewGroup, false);
                        ViewHolder viewHolder = new ViewHolder(view);
                        return viewHolder;
                    }
                };

        mRecyclerViewPremium.setAdapter(mAdapterPremium);
        mAdapterPremium.startListening();

        mAdapterPremium.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = mAdapterPremium.getItemCount();
                int lastVisiblePosition =
                        new LinearLayoutManager(MyPostsActivity.this).findLastCompletelyVisibleItemPosition();
                // If the recycler view is initially being loaded or the
                // user is at the bottom of the list, scroll to the bottom
                // of the list to show the newly added message.
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    mRecyclerViewPremium.scrollToPosition(positionStart);
                }
            }
        });

        queriesPremium.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    mTextNoItemFound.setVisibility(View.GONE);
                    mRecyclerViewPremium.setVisibility(View.VISIBLE);
                }
                else{
                    mTextNoItemFound.setVisibility(View.VISIBLE);
                    mRecyclerViewPremium.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //For simple post
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        Query queriesSimple = mRootRef.child("Posts").child("Simple").orderByChild("userid").equalTo(searchtext);
        FirebaseRecyclerOptions<PostModel> optionsSimple =
                new FirebaseRecyclerOptions.Builder<PostModel>()
                        .setQuery(queriesSimple, PostModel.class)
                        .build();

        mAdapterSimple =
                new FirebaseRecyclerAdapter<PostModel, ViewHolder>(optionsSimple) {
                    @Override
                    protected void onBindViewHolder(@NonNull final ViewHolder holder, final int position, @NonNull final PostModel model) {

                        holder.title.setText(model.getTitle());
                        holder.description.setText(model.getDescription());
                        holder.compaignurl.setText(model.getCompaignurl());
                         mRootRef.child("Users").child(model.getUserid())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()){
                                    String username=dataSnapshot.child("name").getValue().toString();
                                    String userimage=dataSnapshot.child("image").getValue().toString();
                                    holder.username.setText(username);
                                    Picasso.with(MyPostsActivity.this).load(userimage).placeholder(R.drawable.ic_user).into(holder.userimage);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        Picasso.with(MyPostsActivity.this).load(model.getPostimage()).placeholder(R.drawable.ic_image).into(holder.postimage);

                        mRootRef.child("Comments").child(getRef(position).getKey())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            long totalChild=dataSnapshot.getChildrenCount();
                                            holder.commentnumber.setText(""+totalChild);
                                        } else {
                                            holder.commentnumber.setText("0");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                        mRootRef.child("isFavorite").child(getRef(position).getKey())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            long totalChild=dataSnapshot.getChildrenCount();
                                            holder.favoritenumber.setText(""+totalChild);
                                        } else {
                                            holder.favoritenumber.setText("0");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                        mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()){
                                    holder.imagefavorite.setColorFilter(ContextCompat.getColor(MyPostsActivity.this, R.color.red));
                                } else {
                                    holder.imagefavorite.setColorFilter(ContextCompat.getColor(MyPostsActivity.this, R.color.black));
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        holder.mLayoutComment.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent=new Intent(MyPostsActivity.this, CommentActivity.class);
                                intent.putExtra("postid",getRef(position).getKey());
                                intent.putExtra("posttitle",model.getTitle());
                                startActivity(intent);
                            }
                        });
                        holder.imagefavorite.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()){
                                            int getTotalFavorite= Integer.parseInt(holder.favoritenumber.getText().toString());
                                            holder.favoritenumber.setText(String.valueOf(getTotalFavorite-1));
                                            holder.imagefavorite.setColorFilter(ContextCompat.getColor(MyPostsActivity.this, R.color.black));
                                            mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).removeValue();
                                        } else {
                                            int getTotalFavorite= Integer.parseInt(holder.favoritenumber.getText().toString());
                                            holder.favoritenumber.setText(String.valueOf(getTotalFavorite+1));
                                            holder.imagefavorite.setColorFilter(ContextCompat.getColor(MyPostsActivity.this, R.color.red));
                                            mRootRef.child("isFavorite").child(getRef(position).getKey()).child(mCurrentUserId).child("favorite").setValue("yes");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                        });

                        holder.compaignurl.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent browserIntent = new Intent(MyPostsActivity.this, WebViewActivity.class);
                                browserIntent.putExtra("url",model.getCompaignurl());
                                startActivity(browserIntent);
                            }
                        });
                        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View view) {
                                AlertDialog alertDialog = new AlertDialog.Builder(MyPostsActivity.this).create();
                                alertDialog.setTitle("Alerta");
                                alertDialog.setMessage("¿Estás segura de eliminar el artículo?");
                                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "si",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                ShowDialog("Por favor espere eliminar el artículo...");
                                                mRootRef.child("Posts").child("Simple").child(getRef(position).getKey())
                                                        .removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()){
                                                            HideDialog();
                                                            Toast.makeText(MyPostsActivity.this, "Elemento eliminado correctamente", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            HideDialog();
                                                            Toast.makeText(MyPostsActivity.this, "Error", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                alertDialog.show();
                                return false;
                            }
                        });

                    }

                    @NonNull
                    @Override
                    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                        View view = LayoutInflater.from(viewGroup.getContext())
                                .inflate(R.layout.layoutpost, viewGroup, false);
                        ViewHolder viewHolder = new ViewHolder(view);
                        return viewHolder;
                    }
                };

        mRecyclerViewSimple.setAdapter(mAdapterSimple);
        mAdapterSimple.startListening();

        mAdapterSimple.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = mAdapterSimple.getItemCount();
                int lastVisiblePosition =
                        new LinearLayoutManager(MyPostsActivity.this).findLastCompletelyVisibleItemPosition();
                // If the recycler view is initially being loaded or the
                // user is at the bottom of the list, scroll to the bottom
                // of the list to show the newly added message.
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    mRecyclerViewSimple.scrollToPosition(positionStart);
                }
            }
        });

        queriesSimple.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    mTextNoItemFound.setVisibility(View.GONE);
                    mRecyclerViewSimple.setVisibility(View.VISIBLE);
                }
                else{
                    mTextNoItemFound.setVisibility(View.VISIBLE);
                    mRecyclerViewSimple.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }
    //class of view holder for recyclerview to show items
    private static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title, description,username,compaignurl,favoritenumber,commentnumber;
        private CircleImageView userimage;
        private ImageView postimage,imagefavorite;
        private LinearLayout mLayoutFavorite,mLayoutComment;

        public ViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.layoutposttitle);
            description = (TextView) itemView.findViewById(R.id.layoutpostdescription);
            username = (TextView) itemView.findViewById(R.id.layoutusername);
            compaignurl = (TextView) itemView.findViewById(R.id.layoutpostcompaignurl);
            favoritenumber = (TextView) itemView.findViewById(R.id.layoutpostfavoritenumber);
            commentnumber = (TextView) itemView.findViewById(R.id.layoutpostcommentnumber);
            userimage = (CircleImageView) itemView.findViewById(R.id.layoutuserimage);
            postimage = (ImageView) itemView.findViewById(R.id.layoutpostimage);
            imagefavorite = (ImageView) itemView.findViewById(R.id.imagefavorite);
            mLayoutFavorite = (LinearLayout) itemView.findViewById(R.id.layoutfavorite);
            mLayoutComment = (LinearLayout) itemView.findViewById(R.id.layoutcomment);


        }
    }
    private void ShowDialog(String message) {
        if (mLoadingBar != null && !mLoadingBar.isShowing()) {
            mLoadingBar.setCancelable(true);
            mLoadingBar.setMessage(message);
            mLoadingBar.show();
        }
    }

    private void HideDialog() {
        if (mLoadingBar != null && mLoadingBar.isShowing()) {
            mLoadingBar.dismiss();
        }
    }
}
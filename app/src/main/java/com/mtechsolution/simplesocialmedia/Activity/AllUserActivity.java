package com.mtechsolution.simplesocialmedia.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.mtechsolution.simplesocialmedia.Model.UserModel;
import com.mtechsolution.simplesocialmedia.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class AllUserActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private DatabaseReference mRootRef;
    private RecyclerView mRecyclerView;
    private String mCurrentUserId;
    private FirebaseRecyclerAdapter<UserModel, ViewHolder> mAdapter;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_user);

        mToolbar=(Toolbar) findViewById(R.id.toolbar);
        mRecyclerView=(RecyclerView) findViewById(R.id.recyclerviewallusers);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Seleccionar Contactos");
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mAuth=FirebaseAuth.getInstance();
        mCurrentUser=mAuth.getCurrentUser();
        mCurrentUserId=mCurrentUser.getUid();
        mRootRef= FirebaseDatabase.getInstance().getReference();

        mRecyclerView.setHasFixedSize(true);
        //set layout manager to recycler view
        mRecyclerView.setLayoutManager(new LinearLayoutManager(AllUserActivity.this));


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        FirebaseRecyclerOptions<UserModel> options =
                new FirebaseRecyclerOptions.Builder<UserModel>()
                        .setQuery(mRootRef.child("Users"), UserModel.class)
                        .build();

        mAdapter =
                new FirebaseRecyclerAdapter<UserModel, ViewHolder>(options) {
                    @Override
                    protected void onBindViewHolder(@NonNull final ViewHolder holder, final int position, @NonNull final UserModel model) {

                        if (!mCurrentUserId.equals(model.getUserid())){
                            holder.itemView.setVisibility(View.VISIBLE);
                            holder.name.setText(model.getName());
                            holder.phonenumber.setText(model.getEmail());
                            Picasso.with(AllUserActivity.this).load(model.getImage()).placeholder(R.drawable.ic_user).into(holder.image);

                            if (model.getOnline().equals("true")) {
                                holder.online.setImageDrawable(ContextCompat.getDrawable(AllUserActivity.this, R.drawable.dotblue));
                            } else {
                                holder.online.setImageDrawable(ContextCompat.getDrawable(AllUserActivity.this, R.drawable.dotred));
                            }
                        }
                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent=new Intent(AllUserActivity.this,MessageActivity.class);
                                intent.putExtra("name",model.getName());
                                intent.putExtra("image",model.getImage());
                                intent.putExtra("userid",model.getUserid());
                                startActivity(intent);
                            }
                        });

                        holder.setIsRecyclable(false);
                    }

                    @NonNull
                    @Override
                    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                        View view = LayoutInflater.from(viewGroup.getContext())
                                .inflate(R.layout.layoutuser, viewGroup, false);
                        ViewHolder viewHolder = new ViewHolder(view);
                        return viewHolder;
                    }
                };

        mRecyclerView.setAdapter(mAdapter);
        mAdapter.startListening();
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    //class of view holder for recyclerview to show items
    private static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView name,phonenumber;
        private CircleImageView image;
        private ImageView online;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.layoutusername);
            phonenumber = (TextView) itemView.findViewById(R.id.layoutuserphonenumber);
            image = (CircleImageView) itemView.findViewById(R.id.layoutuserimage);
            online = (ImageView) itemView.findViewById(R.id.layoutuseronline);

        }

    }
    private void searchUser(String name){
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        FirebaseRecyclerOptions<UserModel> options =
                new FirebaseRecyclerOptions.Builder<UserModel>()
                        .setQuery(mRootRef.child("Users").child(name), UserModel.class)
                        .build();

        mAdapter =
                new FirebaseRecyclerAdapter<UserModel, ViewHolder>(options) {
                    @Override
                    protected void onBindViewHolder(@NonNull final ViewHolder holder, final int position, @NonNull final UserModel model) {


                        if (mCurrentUserId.equals(model.getUserid())){
                            holder.itemView.setVisibility(View.GONE);
                           holder.name.setVisibility(View.GONE);
                           holder.image.setVisibility(View.GONE);
                           holder.phonenumber.setVisibility(View.GONE);
                           holder.online.setVisibility(View.GONE);
                        } else {
                            holder.itemView.setVisibility(View.VISIBLE);
                            holder.name.setText(model.getName());
                            holder.phonenumber.setText(model.getEmail());
                            Picasso.with(AllUserActivity.this).load(model.getImage()).placeholder(R.drawable.ic_user).into(holder.image);

                            if (model.getOnline().equals("true")) {
                                holder.online.setImageDrawable(ContextCompat.getDrawable(AllUserActivity.this, R.drawable.dotblue));
                            } else {
                                holder.online.setImageDrawable(ContextCompat.getDrawable(AllUserActivity.this, R.drawable.dotred));
                            }
                        }

                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent=new Intent(AllUserActivity.this,MessageActivity.class);
                                intent.putExtra("name",model.getName());
                                intent.putExtra("image",model.getImage());
                                intent.putExtra("userid",model.getUserid());
                                startActivity(intent);
                            }
                        });
                    }

                    @NonNull
                    @Override
                    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                        View view = LayoutInflater.from(viewGroup.getContext())
                                .inflate(R.layout.layoutuser, viewGroup, false);
                        ViewHolder viewHolder = new ViewHolder(view);
                        return viewHolder;
                    }
                };

        mRecyclerView.setAdapter(mAdapter);
        mAdapter.startListening();

//        mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
//            @Override
//            public void onItemRangeInserted(int positionStart, int itemCount) {
//                super.onItemRangeInserted(positionStart, itemCount);
//                int friendlyMessageCount = mAdapter.getItemCount();
//                int lastVisiblePosition =
//                        new LinearLayoutManager(AllUserActivity.this).findLastCompletelyVisibleItemPosition();
//                // If the recycler view is initially being loaded or the
//                // user is at the bottom of the list, scroll to the bottom
//                // of the list to show the newly added message.
//                if (lastVisiblePosition == -1 ||
//                        (positionStart >= (friendlyMessageCount - 1) &&
//                                lastVisiblePosition == (positionStart - 1))) {
//                    mRecyclerView.scrollToPosition(positionStart);
//                }
//            }
//        });
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }
    private void loadUsers(){

    }

}
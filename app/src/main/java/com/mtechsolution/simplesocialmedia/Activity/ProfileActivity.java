package com.mtechsolution.simplesocialmedia.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mtechsolution.simplesocialmedia.R;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private CircleImageView mImageUser;
    private Button mBtnUpdate;
    private EditText mEdtEmail,mEdtDOB,mEdtName;
    private RadioButton mRbMale,mRbFemale;
    private SharedPreferences mSharePref;
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private String mCurrentUserId;
    private DatabaseReference mRootRef;
    private StorageReference mStorageRef;
    private static final int PERMISSION_GRAUNTED=555;
    private ProgressDialog mLoadingBar;
    private Calendar myCalendar;
    private String gender;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mToolbar=(Toolbar) findViewById(R.id.toolbar);
        mBtnUpdate=(Button) findViewById(R.id.btnupdate);
        mEdtEmail=(EditText) findViewById(R.id.edtemail);
        mEdtName=(EditText) findViewById(R.id.edtname);
        mEdtDOB=(EditText) findViewById(R.id.edtdob);
        mRbFemale=(RadioButton) findViewById(R.id.rbfemale);
        mRbMale=(RadioButton) findViewById(R.id.rbmale);
        mImageUser=(CircleImageView) findViewById(R.id.imageuser);


        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Perfil");
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        myCalendar = Calendar.getInstance();
        mLoadingBar=new ProgressDialog(this);
        mSharePref = getSharedPreferences("socialmediaapp", MODE_PRIVATE);
        mAuth=FirebaseAuth.getInstance();
        mCurrentUser=mAuth.getCurrentUser();
        mCurrentUserId=mCurrentUser.getUid();
        mRootRef= FirebaseDatabase.getInstance().getReference();
        mStorageRef = FirebaseStorage.getInstance().getReference().child("User Images");

        Picasso.with(ProfileActivity.this).load(mSharePref.getString("image","null"))
                .placeholder(R.drawable.ic_user).into(mImageUser);
        mEdtName.setText(mSharePref.getString("name","null"));
        mEdtEmail.setText(mSharePref.getString("email","null"));
        mEdtDOB.setText(mSharePref.getString("dob","null"));
        if (mSharePref.getString("gender","null").equals("male")){
            mRbMale.setChecked(true);
            mRbFemale.setChecked(false);
        } else {
            mRbMale.setChecked(false);
            mRbFemale.setChecked(true);
        }



        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        mEdtDOB.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(ProfileActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        mRbMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (mRbMale.isChecked()){
                    mRbFemale.setChecked(false);
                    gender="male";
                } else {
                    mRbFemale.setChecked(true);
                    gender="female";
                }
            }
        });
        mRbFemale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (mRbFemale.isChecked()){
                    mRbMale.setChecked(false);
                    gender="female";
                } else {
                    mRbMale.setChecked(true);
                    gender="male";
                }
            }
        });
        mImageUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckAppPermissions();
            }
        });
        mBtnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowDialog("Por favor espere actualización de información...");
                mCurrentUser = mAuth.getCurrentUser();
                mCurrentUserId = mCurrentUser.getUid();
                mRootRef = FirebaseDatabase.getInstance().getReference();
                Map add = new HashMap<>();
                add.put("name", mEdtName.getText().toString());
                add.put("email", mEdtEmail.getText().toString());
                add.put("dob", mEdtDOB.getText().toString());
                add.put("gender", gender);
                mRootRef.child("Users").child(mCurrentUserId).updateChildren(add)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    SharedPreferences.Editor editor = mSharePref.edit();
                                    editor.putString("name", mEdtName.getText().toString());
                                    editor.putString("dob", mEdtDOB.getText().toString());
                                    editor.putString("email", mEdtEmail.getText().toString());
                                    editor.putString("gender", gender);
                                    editor.apply();
                                    HideDialog();
                                } else {
                                    Toast.makeText(ProfileActivity.this, "Error", Toast.LENGTH_SHORT).show();
                                    HideDialog();
                                }

                            }
                        });
            }
        });
    }
    //create method in main
    private void CheckAppPermissions()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try
            {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA}, PERMISSION_GRAUNTED);
            }
            catch (Exception ex)
            {

            }
        }
        else
        {
            PickImageFromGallery();
        }
    }
    private void PickImageFromGallery()
    {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1,1)
                .start(this);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                ShowDialog("Por favor espera, carga de imagen...");
                final Uri resultUri = result.getUri();

                StorageReference filePath = mStorageRef.child(mCurrentUserId + ".jpg");
                filePath.putFile(resultUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        if (taskSnapshot.getMetadata() != null) {
                            if (taskSnapshot.getMetadata().getReference() != null) {
                                final Task<Uri> result = taskSnapshot.getStorage().getDownloadUrl();
                                result.addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        final String imageUrl = uri.toString();

                                        mRootRef.child("Users").child(mCurrentUserId).child("image").setValue(imageUrl)
                                                .addOnCompleteListener(new OnCompleteListener() {
                                                    @Override
                                                    public void onComplete(@NonNull Task task) {
                                                        if (task.isSuccessful()){
                                                            //set into user detial shared preference
                                                            SharedPreferences.Editor editor=mSharePref.edit();
                                                            editor.putString("image",imageUrl);
                                                            editor.apply();
                                                            HideDialog();
                                                            mImageUser.setImageURI(resultUri);
                                                        } else {
                                                            HideDialog();
                                                            Toast.makeText(ProfileActivity.this, "Error", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                    }
                                });
                            }
                        }
                    }
                });

            }

        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSION_GRAUNTED && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            PickImageFromGallery();
        }
        else
        {
            CheckAppPermissions();
        }
    }
    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        mEdtDOB.setText(sdf.format(myCalendar.getTime()));
    }
    private void ShowDialog(String message) {
        if (mLoadingBar != null && !mLoadingBar.isShowing()) {
            mLoadingBar.setCancelable(true);
            mLoadingBar.setMessage(message);
            mLoadingBar.show();
        }
    }

    private void HideDialog() {
        if (mLoadingBar != null && mLoadingBar.isShowing()) {
            mLoadingBar.dismiss();
        }
    }
}
package com.mtechsolution.simplesocialmedia.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.mtechsolution.simplesocialmedia.R;

public class ForgotPasswordActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private EditText mEdtEmail;
    private Button mBtnSubmit;
    private FirebaseAuth mAuth;
    private ProgressDialog mLoadingBar;

    public ForgotPasswordActivity() {
    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        mToolbar=(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Se te olvidó tu contraseña");
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mAuth=FirebaseAuth.getInstance();
        mLoadingBar=new ProgressDialog(this);

        mEdtEmail=(EditText) findViewById(R.id.edtemail);
        mBtnSubmit=(Button) findViewById(R.id.btnsubmit);
        mBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String getemail=mEdtEmail.getText().toString();
                if (getemail.isEmpty()){
                    Toast.makeText(ForgotPasswordActivity.this, "Por favor, ingrese contraseña", Toast.LENGTH_SHORT).show();
                } else {
                    ForgotPassword(getemail);
                }
            }
        });
    }
    private void ForgotPassword(String email){
        ShowDialog("Espere el restablecimiento de la contraseña ...");
        mAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful())
                        {
                            Toast.makeText(ForgotPasswordActivity.this, "comprobar el correo electrónico para recuperar la cuenta", Toast.LENGTH_LONG).show();
                            HideDialog();
                        }
                        else
                        {
                            Toast.makeText(ForgotPasswordActivity.this, "error en contraseña olvidada", Toast.LENGTH_LONG).show();
                            HideDialog();
                        }
                    }
                });
    }
    private void ShowDialog(String message) {
        if (mLoadingBar != null && !mLoadingBar.isShowing()) {
            mLoadingBar.setCancelable(true);
            mLoadingBar.setMessage(message);
            mLoadingBar.show();
        }
    }

    private void HideDialog() {
        if (mLoadingBar != null && mLoadingBar.isShowing()) {
            mLoadingBar.dismiss();
        }
    }
}
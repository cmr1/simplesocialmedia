package com.mtechsolution.simplesocialmedia.Notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Message;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mtechsolution.simplesocialmedia.Activity.MainActivity;
import com.mtechsolution.simplesocialmedia.Activity.MessageActivity;
import com.mtechsolution.simplesocialmedia.R;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class MyFirebaseMessaging extends FirebaseMessagingService {

    private String sender_id,sender_name,sender_pic,title,message;
    private Bitmap bitmap;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (remoteMessage.getData().size() > 0) {
            sender_id = remoteMessage.getData().get("sender_id");
            sender_name = remoteMessage.getData().get("sender_name");
            sender_pic = remoteMessage.getData().get("sender_pic");

            //To get a Bitmap image from the URL received
            bitmap = getBitmapfromUrl(sender_pic);


        }


        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            title = remoteMessage.getNotification().getTitle(); //get title
            message = remoteMessage.getNotification().getBody(); //get message

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                sendOreoNotification(title,message,sender_id,sender_name,sender_pic);
            } else {
                sendNotification(title,message,sender_id,sender_name,sender_pic);
            }
        }

    }

    private void sendOreoNotification(String title, String message, String id, String name, String pic){

        Intent intent = new Intent(this, MessageActivity.class);
        intent.putExtra("userid",id);
        intent.putExtra("name",name);
        intent.putExtra("image",pic);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        OreoNotification oreoNotification = new OreoNotification(this);
        Notification.Builder builder = oreoNotification.getOreoNotification(title, message,bitmap, pendingIntent,
                defaultSound, String.valueOf(R.mipmap.ic_launcher));


        oreoNotification.getManager().notify(1, builder.build());

    }

    private void sendNotification(String title, String message, String id, String name, String pic) {

        Intent intent = new Intent(this, MessageActivity.class);
        intent.putExtra("userid",id);
        intent.putExtra("name",name);
        intent.putExtra("pic",pic);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setLargeIcon(bitmap)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSound)
                .setContentIntent(pendingIntent);
        NotificationManager noti = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);



        noti.notify(1, builder.build());
    }

    /*
     *To get a Bitmap image from the URL received
     * */
    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }
}

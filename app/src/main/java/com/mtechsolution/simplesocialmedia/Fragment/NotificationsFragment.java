package com.mtechsolution.simplesocialmedia.Fragment;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mtechsolution.simplesocialmedia.Model.NotificationModel;
import com.mtechsolution.simplesocialmedia.R;


public class NotificationsFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private DatabaseReference mRootRef;
    private FirebaseRecyclerAdapter<NotificationModel, ViewHolder> mAdapter;
    private ProgressDialog mLoadingBar;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_notifications, container, false);

        mRecyclerView = (RecyclerView) root.findViewById(R.id.recyclerviewnotification);

        mRootRef = FirebaseDatabase.getInstance().getReference().child("Notifications");
        mLoadingBar = new ProgressDialog(getContext());


        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        FirebaseRecyclerOptions<NotificationModel> options =
                new FirebaseRecyclerOptions.Builder<NotificationModel>()
                        .setQuery(mRootRef, NotificationModel.class)
                        .build();

        mAdapter =
                new FirebaseRecyclerAdapter<NotificationModel, ViewHolder>(options) {
                    @Override
                    protected void onBindViewHolder(@NonNull final ViewHolder holder, final int position, @NonNull final NotificationModel model) {

                        holder.title.setText(model.getTitle());
                        holder.description.setText(model.getDescription());

                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                                alertDialog.setTitle(model.getTitle());
                                alertDialog.setMessage(model.getDescription());
                                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "¡Entendido!",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                alertDialog.show();
                            }
                        });

                    }

                    @NonNull
                    @Override
                    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                        View view = LayoutInflater.from(viewGroup.getContext())
                                .inflate(R.layout.layoutnotification, viewGroup, false);
                        ViewHolder viewHolder = new ViewHolder(view);
                        return viewHolder;
                    }
                };

        mRecyclerView.setAdapter(mAdapter);
        mAdapter.startListening();
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        return root;
    }
    //class of view holder for recyclerview to show items
    private static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title, description;

        public ViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.layoutnotificationtitle);
            description = (TextView) itemView.findViewById(R.id.layoutnotificationdescription);


        }
    }

}
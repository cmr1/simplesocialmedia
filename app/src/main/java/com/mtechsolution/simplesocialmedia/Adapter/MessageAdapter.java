package com.mtechsolution.simplesocialmedia.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.mtechsolution.simplesocialmedia.Model.MessagesModel;
import com.mtechsolution.simplesocialmedia.R;

import java.util.List;


/**
 * Created by FAROOQ on 2019-06-21.
 */

public class MessageAdapter extends RecyclerView.Adapter<com.mtechsolution.simplesocialmedia.Adapter.MessageAdapter.MessageViewHolder> {

    private List<MessagesModel> messagesList;
    private FirebaseAuth auth;
    private int position;
    private String messagepushid;
    private Context ctx;


    public MessageAdapter(List<MessagesModel> messagesList, Context ctx)
    {
        this.messagesList=messagesList;
        this.ctx=ctx;
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutmessage,parent,false);

        auth= FirebaseAuth.getInstance();
        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final @NonNull MessageViewHolder holder,final int position) {

        String sendermessageid=auth.getCurrentUser().getUid();
        MessagesModel messages=messagesList.get(position);

        String fromuserid=messages.getFrom();
        String type=messages.getType();


        if (type.equals("text"))
        {
            holder.receivermessageparent.setVisibility(View.INVISIBLE);
            holder.sendermessageparent.setVisibility(View.INVISIBLE);

            if (fromuserid.equals(sendermessageid))
            {
                holder.sendermessageparent.setVisibility(View.VISIBLE);
                holder.sendmessage.setBackgroundResource(R.drawable.sender_messages_layout);
                holder.sendmessage.setTextColor(Color.BLACK);
                holder.sendmessage.setText(messages.getMessage());
                holder.sendmessagedatetime.setText(messages.getTime()+" "+messages.getDate());

            }
            else
            {
                holder.receivermessageparent.setVisibility(View.VISIBLE);
                holder.receivermessage.setBackgroundResource(R.drawable.receiver_messages_layout);
                holder.receivermessage.setTextColor(Color.BLACK);
                holder.receivermessage.setText(messages.getMessage());
                holder.receivemessagedatetime.setText(messages.getTime()+" "+messages.getDate());
            }
        }

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setPosition(holder.getAdapterPosition());
                setPushIdPositions(messagesList.get(position).getMessagepushid());
                return false;
            }
        });
        // for resolve the double appear sms automatically
        holder.setIsRecyclable(false);

    }

    public String getPushIdPositions() {
        return messagepushid;
    }

    public void setPushIdPositions(String messagepushid) {
        this.messagepushid=messagepushid;
    }

    public int getPositions() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public int getItemCount() {
        return messagesList.size();
    }


    public class MessageViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener
    {
        private TextView sendmessage,receivermessage,sendmessagedatetime,receivemessagedatetime;
        private LinearLayout receivermessageparent;
        private LinearLayout sendermessageparent;


        public MessageViewHolder(View itemView) {
            super(itemView);

            sendmessage=(TextView) itemView.findViewById(R.id.layoutmessagesendermesssagetext);
            receivermessage=(TextView) itemView.findViewById(R.id.layoutmessagereceivermessagetext);
            sendmessagedatetime=(TextView) itemView.findViewById(R.id.layoutmessagesendermessagedatetime);
            receivemessagedatetime=(TextView) itemView.findViewById(R.id.layoutmessagereceivermessagedatetime);
            receivermessageparent=(LinearLayout) itemView.findViewById(R.id.layoutmessagelineaerlayoutreceicermessage);
            sendermessageparent=(LinearLayout) itemView.findViewById(R.id.layoutmessagelineaerlayoutsendermessage);
            itemView.setOnCreateContextMenuListener(this);

        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            //menuInfo is null
            menu.add(Menu.NONE, 0,
                    Menu.NONE, "Eliminar de cada uno");
            menu.add(Menu.NONE, 1,
                    Menu.NONE, "Eliminar de mí");
        }
    }
}

package com.mtechsolution.simplesocialmedia.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import com.mtechsolution.simplesocialmedia.R;

public class SplashActivity extends AppCompatActivity {

    private static final int PERMISSION_GRAUNTED=555;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        CheckAppPermission();

    }

    private void CheckAppPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                requestPermissions(new String[]{android.Manifest.permission.INTERNET,
                        android.Manifest.permission.ACCESS_NETWORK_STATE}, PERMISSION_GRAUNTED);
            } catch (Exception ex) {

            }
        } else {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent=new Intent(SplashActivity.this,MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, 3000);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSION_GRAUNTED && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent=new Intent(SplashActivity.this,MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, 3000);
        }
        else
        {
            CheckAppPermission();
        }
    }

}